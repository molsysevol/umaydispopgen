#! /usr/bin/python
# Concatenate alignments, output breakpoints and ordered list of sequence ids.

from Bio import AlignIO
from os import listdir
from os.path import join

breakpoints = open("ismc_breakpoints.txt", 'w')
sequenceids = open("ismc_sequenceids.txt", 'w')
inputaligns = open("ismc_alignments.txt", 'w')

files = [f for f in listdir("Alignments") if f.endswith(".fasta")]
files = sorted(files)
for f in files:
  inputaligns.write("%s\n" % f)

aln = AlignIO.read(join("Alignments/", files.pop(0)), "fasta")
aln.sort()
breakpoints.write("%i,%i\n" % (1, len(aln[0].seq)))
for file in files:
  tmp = AlignIO.read(join("Alignments/", file), "fasta")
  tmp.sort()
  a = len(aln[0].seq)
  aln = aln + tmp
  b = len(aln[0].seq)
  breakpoints.write("%i,%i\n" % (a + 1, b))

AlignIO.write(aln, "ismc_alignment.fasta", "fasta")
for record in aln:
  sequenceids.write(record.id + "\n")

breakpoints.close()
sequenceids.close()
inputaligns.close()

print("Done.")

