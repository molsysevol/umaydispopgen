#! /usr/bin/python

# Convert an alignment into a single-block MAF file.
import sys
from Bio import AlignIO

# a1 locus:

a1 = AlignIO.read(open("a1-Genes/cds_1-1-1656-1779.fasta"), "fasta")
a2 = AlignIO.read(open("a1-Genes/cds_2-1-3603-3721.fasta"), "fasta")
a3 = AlignIO.read(open("a1-Genes/cds_3-1-3806-4242.fasta"), "fasta")
a4 = AlignIO.read(open("a1-Genes/cds_4-1-4315-4633.fasta"), "fasta")
a5 = AlignIO.read(open("a1-Genes/cds_5-1-4712-4914.fasta"), "fasta")
a6 = AlignIO.read(open("a1-Genes/cds_6-1-5626-5671.fasta"), "fasta")
a7 = AlignIO.read(open("a1-Genes/cds_7-1-5861-5901.fasta"), "fasta")
a8 = AlignIO.read(open("a1-Genes/cds_8-1-5994-6269.fasta"), "fasta")

a1.sort()
a2.sort()
a3.sort()
a4.sort()
a5.sort()
a6.sort()
a7.sort()
a8.sort()

with open("a1-Genes/mfa1.fasta", 'w') as handle:
    AlignIO.write(a1, handle, "fasta")

a = a5 + a4
a = a + a3
a = a + a2

with open("a1-Genes/pra1.fasta", 'w') as handle:
    AlignIO.write(a, handle, "fasta")

a = a8 + a7
a = a + a6

with open("a1-Genes/rba1.fasta", 'w') as handle:
    AlignIO.write(a, handle, "fasta")


# a2 locus:

a1 = AlignIO.read(open("a2-Genes/cds_1-1-4094-4197.fasta"), "fasta")
a2 = AlignIO.read(open("a2-Genes/cds_2-1-4299-4705.fasta"), "fasta")
a3 = AlignIO.read(open("a2-Genes/cds_3-1-4773-5305.fasta"), "fasta")
a4 = AlignIO.read(open("a2-Genes/cds_4-1-5746-5781.fasta"), "fasta")
a5 = AlignIO.read(open("a2-Genes/cds_5-1-5858-6111.fasta"), "fasta")
a6 = AlignIO.read(open("a2-Genes/cds_6-1-6179-6271.fasta"), "fasta")
a7 = AlignIO.read(open("a2-Genes/cds_7-1-6357-6507.fasta"), "fasta")
a8 = AlignIO.read(open("a2-Genes/cds_8-1-6575-6693.fasta"), "fasta")
a9 = AlignIO.read(open("a2-Genes/cds_9-1-7380-7682.fasta"), "fasta")
a10 = AlignIO.read(open("a2-Genes/cds_10-1-7765-7940.fasta"), "fasta")
a11 = AlignIO.read(open("a2-Genes/cds_11-1-8892-9009.fasta"), "fasta")
a12 = AlignIO.read(open("a2-Genes/cds_12-1-9838-10117.fasta"), "fasta")

a1.sort()
a2.sort()
a3.sort()
a4.sort()
a5.sort()
a6.sort()
a7.sort()
a8.sort()
a9.sort()
a10.sort()
a11.sort()
a12.sort()

a = a3 + a2
a = a + a1

with open("a2-Genes/pra2.fasta", 'w') as handle:
    AlignIO.write(a, handle, "fasta")

a = a8 + a7
a = a + a6
a = a + a5
a = a + a4

with open("a2-Genes/lga2.fasta", 'w') as handle:
    AlignIO.write(a, handle, "fasta")

a = a9 + a10

with open("a2-Genes/rga2.fasta", 'w') as handle:
    AlignIO.write(a, handle, "fasta")

with open("a2-Genes/mfa2.fasta", 'w') as handle:
    AlignIO.write(a11, handle, "fasta")

with open("a2-Genes/rba2.fasta", 'w') as handle:
    AlignIO.write(a12, handle, "fasta")


