# Analyses of Linkage Disequilibrium

Created on 08/01/19 by jdutheil

Measures of LD in U. maydis
===========================

Get only SNPs with MAF > 0.2
```bash
vcftools --gzvcf ../Umaydis.snp.vcf.gz --out Umaydis.snp.maf20 --remove-filtered-all --maf 0.2 --recode
bgzip Umaydis.snp.maf20.recode.vcf
```

Then we filter SNPs and compute LD in windows.
We need to randomly sample SNPs, keeping only 10%:
```bash
./sampleSNPs.sh Umaydis.snp.maf20.recode.vcf.gz Umaydis.snp.maf20.sampled.vcf 0.1
bgzip Umaydis.snp.maf20.sampled.vcf

vcftools --gzvcf Umaydis.snp.maf20.sampled.vcf.gz --out Umaydis.snp.maf20.ld --hap-r2 --ld-window-bp 250000
```

We do the same but keeping only Mexican strains:

```bash
vcftools --gzvcf ../Umaydis.snp.vcf.gz --remove-indv Umaydis_ref --out Umaydis.snp.mexicans.maf20 --remove-filtered-all --maf 0.2 --recode
bgzip Umaydis.snp.mexicans.maf20.recode.vcf
```

We then need to randomly sample SNPs, keeping only 10%:
```bash
./sampleSNPs.sh Umaydis.snp.mexicans.maf20.recode.vcf.gz Umaydis.snp.mexicans.maf20.sampled.vcf 0.10
bgzip Umaydis.snp.mexicans.maf20.sampled.vcf

vcftools --gzvcf Umaydis.snp.mexicans.maf20.sampled.vcf.gz --out Umaydis.snp.mexicans.maf20.ld --hap-r2 --ld-window-bp 250000
```

Compare with humans:
====================

Extract a 20 Mb region from chr9 for 11 individuals (22 haplotypes):

```bash  
vcftools --gzvcf chr9.snp_only.recode.vcf.gz --chr 9 --from-bp 2000000 --to-bp 22000000 --maf 0.0001 --out Hsapiens.snp --max-indv 11 --recode
bgzip Hsapiens.snp.recode.vcf
```

Get only SNPs with MAF > 0.2
```bash
vcftools --gzvcf Hsapiens.snp.recode.vcf.gz --out Hsapiens.snp.maf20 --remove-filtered-all --maf 0.2 --recode
bgzip Hsapiens.snp.maf20.recode.vcf
```

Sample 10% of SNPs:
```
./sampleSNPs.sh Hsapiens.snp.maf20.recode.vcf.gz Hsapiens.snp.maf20.recode.sampled.vcf 0.10
bgzip Hsapiens.snp.maf20.recode.sampled.vcf

vcftools --gzvcf Hsapiens.snp.maf20.recode.sampled.vcf.gz --out Hsapiens.snp.maf20.ld --hap-r2 --ld-window-bp 250000
```

And with Z. tritici:
====================

Get only SNPs with MAF > 0.2 and unrelated individuals:
```bash
vcftools --gzvcf tba_refZt09_windows_realigned_cleaned.snp.vcf.gz --out Ztritici.snp.maf20 \
           --remove-filtered-all \
           --remove-indv Zt148 \
           --remove-indv Zt151 \
           --remove-indv Zt153 \
           --maf 0.2 --recode
bgzip Ztritici.snp.maf20.recode.vcf
```

We sample 1% of SNPs:
```bash
./sampleSNPs.sh Ztritici.snp.maf20.recode.vcf.gz Ztritici.snp.maf20.recode.sampled.vcf 0.01
bgzip Ztritici.snp.maf20.recode.sampled.vcf

vcftools --gzvcf Ztritici.snp.maf20.recode.sampled.vcf.gz --out Ztritici.snp.maf20.ld --hap-r2 --ld-window-bp 250000
```

* Analysis.R for statistical analyses and graphs

