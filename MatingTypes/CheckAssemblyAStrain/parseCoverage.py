#! /usr/bin/python

import gzip
import sys
import re
from collections import Counter

# This provides a detailed, per bp info:
"""
with gzip.open(sys.argv[1], "rt") as input_handle:
    for line in input_handle:
        values = line.split("\t")
        chr = values[0]
        pos = values[1]
        ref = values[2]
        seq = values[4]
        seq2 = re.sub(r'[^ACGTacgt\\,\\.]', '', seq)
        seq3 = re.sub(r'[\\,\\.]', ref, seq2)
        coverage = len(seq3)
        counts = Counter(seq3)
        print("%s\t%s\t%s\t%s\t%s\t%s\t%s" % (chr, pos, coverage, counts['A'], counts['C'], counts['G'], counts['T']))
"""

# We provide a summary per chromosome/scaffold:
print("Chr\tNbPos\tAvgCov\tNbAlt\tAvgAlt")
with gzip.open(sys.argv[1], "rt") as input_handle:
    current_chr = ""
    nb_pos = 0
    nb_alt = 0
    sum_cov = 0
    sum_alt = 0
    for line in input_handle:
        values = line.split("\t")
        chr = values[0]
        pos = values[1]
        ref = values[2]
        seq = values[4]
        seq2 = re.sub(r'[^ACGTacgt\\,\\.]', '', seq)
        seq3 = re.sub(r'[\\,\\.]', ref, seq2)
        coverage = len(seq3)
        counts = list(Counter(seq3).values())
        counts.sort(reverse = True)
        if chr != current_chr:
            if current_chr != "":
                # Print results for previous chr:
                if nb_pos > 0:
                    print ("%s\t%s\t%s\t%s\t%s" % (current_chr, nb_pos, sum_cov/nb_pos, nb_alt, sum_alt/nb_pos))
            # Reset variables:
            current_chr = chr
            nb_pos = 0
            nb_alt = 0
            sum_cov = 0
            sum_alt = 0
        if len(counts) > 0:
            nb_pos = nb_pos + 1
            sum_cov = sum_cov + coverage
            alt_count = (coverage - counts[0]) / coverage
            sum_alt  = sum_alt + alt_count
            if alt_count > 0.2:
                nb_alt = nb_alt + 1


