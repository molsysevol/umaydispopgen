# created by Gabriel on Feb 10, 2018
# last modified by Gabriel on Feb 10, 2018
# last modified by Gabriel on May 15, 2019

# Here, I add the Sreilianum sequence to the extracted concatenated CDS sequences of Umaydis.
# The link is provided with a SiLiX analysis.

import os
from Bio import SeqIO

srseq = open("Sporez_cds_raw.fa")
srrec = list(SeqIO.parse(srseq, "fasta"))
single_genes = open("SiLiX/Blast/MissingBlast/BestSingleHits.txt")

lst = []
for line in single_genes.readlines():
	sr_seq = ""
	sr_id = ""
	l = line.rstrip()
	if l.split(" ")[0] != "UmGene":
		um_file = "Output/Combine/" + l.split(" ")[0] + ".fa"
		if os.path.isfile(um_file):
			umseq = open(um_file)
			umrec = list(SeqIO.parse(umseq, "fasta"))
			for r1 in range(len(srrec)):
				if (srrec[r1].id).split("@")[1] == l.split(" ")[1]:
					sr_seq = str(srrec[r1].seq)
					sr_id = l.split(" ")[1]
					lst.append(l.split(" ")[0] + "_" + sr_id)
					with open("Output/Combine/AddGenes/Homologs2/" + l.split(" ")[0] + "_" + sr_id + ".fa", "w") as out_file:
						for u in range(len(umrec)):
							um_seq_out = (str(umrec[u].seq)).replace("-", "")
							out_file.write(">" + umrec[u].id + "\n" + um_seq_out + "\n")
						out_file.write(">" + "Sreilianum" + "\n" + sr_seq + "\n")

with open("Output/Combine/AddGenes/Homologs2/GeneList2.txt", "w") as of:
	for e in lst:
		of.write(e + "\n")
