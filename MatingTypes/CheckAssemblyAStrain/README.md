<!-- Created on 29/09/20 by jdutheil -->

Map all reads on the genome assembly and assess the amount of possible heterozygotes and general coverage.
We compare strain A to strain B and C

```bash
cp ../../Assemblies/StrainA .
bwa index StrainA
cp ../../Assemblies/StrainB .
bwa index StrainB
cp ../../Assemblies/StrainC .
bwa index StrainC
```

Map all lanes on the assembly:
```bash
MERGED_DIR="/mnt/data2/jdutheil/Data/Fungi/UmaydisPopGen/Merged/"

function map_each_lane() {
  echo "Mapping $1 on $2"
  for i in $(find $MERGED_DIR/$1/ -type f -name "*.fastq.gz" | while read F; do basename $F | rev | cut -c 13- | rev; done | sort | uniq)
  do
    echo "Mapping lane $i"
    IFS="_" read -a str <<< $i
    bwa mem -M -t 10 -R "@RG\tID:${str[2]}.${str[3]}\tSM:${str[1]}" $2 $MERGED_DIR/$1/${i}_R1.fastq.gz $MERGED_DIR/$1/${i}_R2.fastq.gz | samtools sort -@8 -o Mapping/$1/${i}.aln-pe.bam
  done
}

mkdir -p Mapping/strainA
map_each_lane strainA StrainA
mkdir -p Mapping/strainB
map_each_lane strainB StrainB
mkdir -p Mapping/strainC
map_each_lane strainC StrainC




samtools index Mapping/strainA/467_A_ATCACGAT_L004.aln-pe.bam
samtools index Mapping/strainA/467_A_ATCACG_L005.aln-pe.bam 
samtools index Mapping/strainB/467_B_TTAGGC_L005.aln-pe.bam
samtools index Mapping/strainC/467_C_ACTTGAAT_L004.aln-pe.bam
samtools index Mapping/strainC/467_C_ACTTGA_L005.aln-pe.bam
```

Look a variation for each position:
```bash
samtools mpileup -f StrainA Mapping/strainA/467_A_ATCACGAT_L004.aln-pe.bam Mapping/strainA/467_A_ATCACG_L005.aln-pe.bam -o mpileup_A.txt
gzip mpileup_A.txt
samtools mpileup -f StrainB Mapping/strainB/467_B_TTAGGC_L005.aln-pe.bam -o mpileup_B.txt
gzip mpileup_B.txt
samtools mpileup -f StrainC Mapping/strainC/467_C_ACTTGAAT_L004.aln-pe.bam Mapping/strainC/467_C_ACTTGA_L005.aln-pe.bam -o mpileup_C.txt
gzip mpileup_C.txt

```

Now parse the results:
```bash
python3 parseCoverage.py mpileup_A.txt.gz > coverage_scaffolds_A.tsv
python3 parseCoverage.py mpileup_B.txt.gz > coverage_scaffolds_B.tsv
python3 parseCoverage.py mpileup_C.txt.gz > coverage_scaffolds_C.tsv
```

Now compare the proportion of ambiguous positions per scaffold:
```r
library(ggplot2)
library(cowplot)
  
datA <- read.table("coverage_scaffolds_A.tsv", sep = "\t", header = TRUE, stringsAsFactors = FALSE)
datA$Prop <- datA$NbAlt / datA$NbPos
datA$Strain <- "A"
hist(datA$Prop)
summary(datA$Prop)
hist(datA$AvgAlt)
summary(datA$AvgAlt)

datB <- read.table("coverage_scaffolds_B.tsv", sep = "\t", header = TRUE, stringsAsFactors = FALSE)
datB$Prop <- datB$NbAlt / datB$NbPos
datB$Strain <- "B"
hist(datB$Prop)
summary(datB$Prop)
boxplot(datA$AvgCov, datB$AvgCov, log="y")

datC <- read.table("coverage_scaffolds_C.tsv", sep = "\t", header = TRUE, stringsAsFactors = FALSE)
datC$Prop <- datC$NbAlt / datC$NbPos
datC$Strain <- "C"
hist(datC$Prop)
summary(datC$Prop)

dat <- rbind(datA, datB, datC)

p.alt <- ggplot(dat, aes(x = Strain, y = AvgAlt)) +
    geom_boxplot() + ylab("Average proportion, per site,\nof alternative nucleotide") +
    theme_bw()
p.alt

p.pro <- ggplot(dat, aes(x = Strain, y = Prop)) +
    geom_boxplot() + ylab("Average proportion, per site,\nof alternative nucleotide") +
    scale_y_log10() +
    theme_bw()
p.pro

sapply(split(dat$Prop, dat$Strain), mean)
datA[grep("scaffold79", datA$Chr),]

p.cov <- ggplot(dat, aes(x = Strain, y = AvgCov)) +
    geom_boxplot() + ylab("Average coverage, per site") +
    scale_y_log10() +
    theme_bw() 
p.cov

p <- plot_grid(p.cov, p.alt, align = "b", labels = "AUTO")
ggsave(p, filename = "Coverage.png", width = 12, height = 6)

sum(datA$NbAlt) / sum(datA$NbPos)
sum(datB$NbAlt) / sum(datB$NbPos)
sum(datC$NbAlt) / sum(datC$NbPos)
```

Look along scaffold79 of strain A:
```
python3 parseCoverageLocal.py mpileup_A.txt.gz StrainA:scaffold79:1:+:90979 > coverage_A_scaffold79.tsv
```

```
datA79 <- read.table("coverage_A_scaffold79.tsv", header = TRUE, stringsAsFactors = FALSE)
datA79$Region <- NA
datA79[datA79$Pos < 76000 & datA79$Pos > 1500, "Region"] <- "N"
datA79[datA79$Pos >= 76500 & datA79$Pos < 83194, "Region"] <- "a2"
datA79[datA79$Pos >= 83194 & datA79$Pos < 88975, "Region"] <- "a1"

require(ggplot2)

p.scaf <- ggplot(datA79, aes(y=Coverage, x=Pos, color = Region)) + 
    geom_point(alpha = 0.5, size = 0.3) +
    geom_smooth(aes(group=Region), data = na.omit(datA79), method="lm") +
    theme_bw() + theme(legend.position = "left") +
    ylim(c(0, 100)) + xlab("Position along scaffold 79")
p.scaf

p.bw <- ggplot(na.omit(datA79), aes(x = Region, y = Coverage, color = Region)) +
    geom_boxplot() + ylim(c(0,100)) +
    theme_bw() + theme(legend.position = "none")
p.bw

require(cowplot)
p <- plot_grid(p.scaf, p.bw, labels = "AUTO", align = "b", rel_widths = c(3, 1))
p

ggsave(p, filename = "Scaffold79.png", width = 12, height = 6)

sapply(split(datA89$Coverage, datA89$Region), median)
wilcox.test(datA89$Coverage[datA89$Region == "N"], mu = 34+15)

```

