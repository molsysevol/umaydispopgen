# created by Gabriel on Jul 18, 2018
# last modified by Gabriel on Jul 18, 2018
# last modified by Gabriel on Jan 18, 2019: change input/output paths to version of Nov2018
# last modified by Gabriel on May 14, 2019: change input/output paths to version of May2019
# last modified by Julien on March 18, 2020: input alignment was merged after filtereing. Change input/output paths to version of March2020

# Here, I summarize all names of files with cds fasta sequences in one list.

import os

d = "Output/"
with open(d + "FileListOnlyUm_March2020.txt", "w") as out_file:
    out_file.write("Number,Chr,Start,End\n")
    for f in os.listdir(d):
        if f.startswith("cds_") and f.endswith(".fasta"):
            x = f.split("-")
            n = x[0].split("_")[1]
            e = x[3].split(".")[0]
            out_file.write(n + "," + x[1] + "," + x[2] + "," + e + "\n")
