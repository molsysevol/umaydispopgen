# iSMC analyses

Created on 20/10/18 by jdutheil

Prepare the input files
=======================

```bash
mkdir Alignments
maffilter param=MafFilter-iSMC.bpp
```

We ignore unmapped contigs:
```bash
mkdir Alignments/Unmapped
mv Alignments/*scaffold* Alignments/Unmapped/
```

Concatenate all alignments and generate breakpoints:
```bash
python formatSeqs.py
```

Process the results in R and generate tab file for iSMC:
```r
d<-read.table("ismc_breakpoints.txt", sep = ",")
t<-data.frame(id=1:nrow(d), start=1, stop=d$V2 - d$V1 + 1, v4=0, v5=d$V2 - d$V1, v6=0, v7=d$V2 - d$V1)
write.table(t, file = "Umaydis.tab", row.names = F, col.names = F, sep ="\t")
```

For simulation purposes, we also compute the nucleotide diversity for each subpop in 10 kb windows.
This was done in the PopDiversity directory, but only in regions without break or missing data.
We redo it here for the merged alignment used as input by iSMC.

```bash
maffilter param=MafFilter-iSMC-Diversity.bpp
```



Run iSMC analyses
=================

Tests with sample DEFGIKMNO. Indices: 3,4,5,6,8,10,12,13,14

All pairs:
```r
x<-c(3,4,5,6,8,10,12,13,14)
for (i in 1:(length(x)-1)) {
  for(j in (i+1):length(x)) {
    cat(x[i], ",", x[j], ",", sep = "")
  }
}
```

Each individual with another one, not the same twice:
```r
x<-c(3,4,5,6,8,10,12,13,14)
y<-c(x[-1], x[1])
paste(x, y, sep = ",", collapse = ",")
```

```bash
OUTDIR=Results/DEFGIKMNO/Gamma/OneKnot
mkdir -p $OUTDIR
ismc param=ismc_opt_gamma.bpp ALN=$OUTDIR/Umaydis_STU "diploid_indices=(3,4,4,5,5,6,6,8,8,10,10,12,12,13,13,14,14,3)" "init_number_knots=1" "max_number_knots=1"
```
Problem: M and K are too close, some chromosomes have no SNP! We only consider K and ignore M.
Each individual with another one, not the same twice:

```r
x<-c(3,4,5,6,8,10,13,14)
y<-c(x[-1], x[1])
paste(x, y, sep = ",", collapse = ",")
```

```bash
OUTDIR=Results/DEFGIKNO/Gamma/OneKnot
mkdir -p $OUTDIR
ismc param=ismc_opt_gamma.bpp ALN=$OUTDIR/Umaydis_DEFGIKNO "diploid_indices=(3,4,4,5,5,6,6,8,8,10,10,13,13,14,14,3)" "init_number_knots=1" "max_number_knots=1"

OUTDIR=Results/DEFGIKNO/Gamma/TwoKnots
mkdir -p $OUTDIR
ismc param=ismc_opt_gamma.bpp ALN=$OUTDIR/Umaydis_DEFGIKNO "diploid_indices=(3,4,4,5,5,6,6,8,8,10,10,13,13,14,14,3)" "init_number_knots=2" "max_number_knots=2"

OUTDIR=Results/DEFGIKNO/Gamma/ThreeKnots
mkdir -p $OUTDIR
ismc param=ismc_opt_gamma.bpp ALN=$OUTDIR/Umaydis_DEFGIKNO "diploid_indices=(3,4,4,5,5,6,6,8,8,10,10,13,13,14,14,3)" "init_number_knots=3" "max_number_knots=3"

OUTDIR=Results/DEFGIKNO/Gamma/FourKnots
mkdir -p $OUTDIR
ismc param=ismc_opt_gamma.bpp ALN=$OUTDIR/Umaydis_DEFGIKNO "diploid_indices=(3,4,4,5,5,6,6,8,8,10,10,13,13,14,14,3)" "init_number_knots=4" "max_number_knots=4"

OUTDIR=Results/DEFGIKNO/Gamma/FiveKnots
mkdir -p $OUTDIR
ismc param=ismc_opt_gamma.bpp ALN=$OUTDIR/Umaydis_DEFGIKNO "diploid_indices=(3,4,4,5,5,6,6,8,8,10,10,13,13,14,14,3)" "init_number_knots=5" "max_number_knots=5"
```

Results:

```
1 knot: AIC = 1282747.951187
Total execution time: 0.000000d, 1.000000h, 47.000000m, 51.000000s.

2 knots: AIC = AIC = 1281057.459347
Total execution time: 0.000000d, 4.000000h, 7.000000m, 31.000000s.

3 knots: AIC = 1280307.265571
Total execution time: 0.000000d, 4.000000h, 56.000000m, 6.000000s.

4 knots: AIC = 1279920.010211
Total execution time: 0.000000d, 4.000000h, 21.000000m, 2.000000s.

5 knots: AIC = AIC = 1279613.991820
Total execution time: 0.000000d, 8.000000h, 41.000000m, 13.000000s.
```

* AnalysisDemography.R for visualizing the results. Seems like 2 knots is enough to capture the trend.



## Comparison with a homogeneous model:


```bash
OUTDIR=Results/DEFGIKNO/Homogeneous/TwoKnots
mkdir -p $OUTDIR
ismc param=ismc_opt_homogeneous.bpp "ALN=$OUTDIR/Umaydis_DEFGIKNO" "diploid_indices=(3,4,4,5,5,6,6,8,8,10,10,13,13,14,14,3)" "init_number_knots=2" "max_number_knots=2"
```
```
AIC = 1286154.592467
Total execution time: 0.000000d, 0.000000h, 4.000000m, 42.000000s.
```

Other population:
```bash
OUTDIR=Results/ABCPQRSTUV/Homogeneous/TwoKnots
mkdir -p $OUTDIR
ismc param=ismc_opt_homogeneous.bpp "ALN=$OUTDIR/Umaydis_ABCPQRSTUV" "diploid_indices=(0,1,1,2,2,15,15,16,16,17,17,18,18,19,19,20,20,21,21,0)" "init_number_knots=2" "max_number_knots=2"
```
```
AIC = 2405845.559031
Total execution time: 0.000000d, 0.000000h, 6.000000m, 37.000000s.
```

Decode all subpops
==================

We use 10 gamma categories and 40 time intervals.

First population:
```bash
OUTDIR=ResultsDecoding/DEFGIKNO/Gamma/TwoKnots
mkdir -p $OUTDIR
ismc param=ismc_opt_gamma_decode.bpp --noninteractive=yes \
     "ALN=$OUTDIR/Umaydis_DEFGIKNO" \
     "diploid_indices=(3,4,4,5,5,6,6,8,8,10,10,13,13,14,14,3)" >& ismc_pop1_post_10c.log
```
AIC = 1280876.466312


Second population:

```bash
OUTDIR=ResultsDecoding/ABCPQRSTUV/Gamma/TwoKnots
mkdir -p $OUTDIR
ismc param=ismc_opt_gamma_decode.bpp --noninteractive=yes \
     "ALN=$OUTDIR/Umaydis_ABCPQRSTUV" \
     "diploid_indices=(0,1,1,2,2,15,15,16,16,17,17,18,18,19,19,20,20,21,21,0)" >& ismc_pop2_post_10c.log
```
AIC = 2397887.575264

Run mapper:

```bash
OUTDIR=ResultsDecoding/DEFGIKNO/Gamma/TwoKnots
ismc_mapper param=ismc_map.bpp "ALN=$OUTDIR/Umaydis_DEFGIKNO"
```

Second population:
```bash
OUTDIR=ResultsDecoding/ABCPQRSTUV/Gamma/TwoKnots
ismc_mapper param=ismc_map.bpp "ALN=$OUTDIR/Umaydis_ABCPQRSTUV"
```

We rerun mapper with smaller window sizes to look around the mating type loci:
```bash
OUTDIR=ResultsDecoding/ABCPQRSTUV/Gamma/TwoKnots
ismc_mapper param=ismc_map.bpp "ALN=$OUTDIR/Umaydis_ABCPQRSTUV" "bin_sizes=1000"
```

Analyze results:

* AnalysisDecoding.R


Rerun after completely masking the mating type loci
===================================================

```bash
mkdir AlignmentsMtMasked
maffilter param=MafFilter-iSMC-MtMasked.bpp
```

We ignore unmapped contigs:
```bash
mkdir AlignmentsMtMasked/Unmapped
mv AlignmentsMtMasked/*scaffold* AlignmentsMtMasked/Unmapped/
```

Concatenate all alignments and generate breakpoints:
```bash
python formatSeqsMtMasked.py
```


Process the results in R and generate tab file for iSMC:
```r
d<-read.table("ismc_mtmasked_breakpoints.txt", sep = ",")
t<-data.frame(id=1:nrow(d), start=1, stop=d$V2 - d$V1 + 1, v4=0, v5=d$V2 - d$V1, v6=0, v7=d$V2 - d$V1)
write.table(t, file = "Umaydis-MtMasked.tab", row.names = F, col.names = F, sep ="\t")
```
Note: this is the same tabfile as before. This is because we indexed everything according to the reference genome, removing alignment columns where there was a gap in the reference genome.

First population: (TODO)
```bash
OUTDIR=ResultsDecodingMtMasked/DEFGIKNO/Gamma/TwoKnots
mkdir -p $OUTDIR
ismc param=ismc_opt_gamma_decode.bpp --noninteractive=yes \
     "ALN=$OUTDIR/Umaydis_DEFGIKNO" \
     "tab_file_path=Umaydis-MtMasked.tab" \
     "diploid_indices=(3,4,4,5,5,6,6,8,8,10,10,13,13,14,14,3)" \
     "sequence_file_path=ismc_mtmasked_alignment.fasta" >& ismc_pop1_mtmasked.log

```

Second population:
```bash
OUTDIR=ResultsDecodingMtMasked/ABCPQRSTUV/Gamma/TwoKnots
mkdir -p $OUTDIR
ismc param=ismc_opt_gamma_decode.bpp --noninteractive=yes \
     "ALN=$OUTDIR/Umaydis_ABCPQRSTUV" \
     "tab_file_path=Umaydis-MtMasked.tab" \
     "diploid_indices=(0,1,1,2,2,15,15,16,16,17,17,18,18,19,19,20,20,21,21,0)" \
     "sequence_file_path=ismc_mtmasked_alignment.fasta" >& ismc_pop2_mtmasked.log

```

Run mapper:

```bash
OUTDIR=ResultsDecodingMtMasked/DEFGIKNO/Gamma/TwoKnots
ismc_mapper param=ismc_map.bpp "ALN=$OUTDIR/Umaydis_DEFGIKNO" "tab_file_path=Umaydis-MtMasked.tab"

OUTDIR=ResultsDecodingMtMasked/ABCPQRSTUV/Gamma/TwoKnots
ismc_mapper param=ismc_map.bpp "ALN=$OUTDIR/Umaydis_ABCPQRSTUV" "tab_file_path=Umaydis-MtMasked.tab"
```

