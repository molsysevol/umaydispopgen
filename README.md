# Created on 17/01/18 by Gabriel Schweizer & Julien Dutheil

Prepare alignment:
==================

Realign locally:
```bash
maffilter param=MafFilter-Realign.bpp
```

Clean the alignment to remove ambiguous regions:
```bash
maffilter param=MafFilter-Clean.bpp
```
This also creates the mask file.

Get general patterns of diversity:
==================================

* Diversity sub-directory

Patterns of linkage disequilibrium
==================================

First we need to export to VCF:
```bash
maffilter param=MafFilter-toVCF.bpp
bgzip Umaydis.snp.vcf
tabix Umaydis.snp.vcf.gz
```
* LD sub-directory for linkage analyses

Population structure:
=====================

* Structure sub-directory for population structure analyses.

Demography analyses:
====================

* MSMC sub-directory for MSMC analyses
* iSMC sub-directory for iSMC analyses

A second look at genetic diversity:
===================================

(based on the patterns of population structure)

* PopDiversity sub-directory

Recombination:
==============

```bash
maffilter param=MafFilter-Merge.bpp
```

* Recombination sub-directory

Gene extraction:
================

* Sub-directory genes


DFEanalyses:
============

* DFE sub-directory

