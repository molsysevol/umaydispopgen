# Created on 17/01/18 by Gabriel Schweizer & Julien Dutheil

We first extract all U. maydis sequences from the alignment:
```bash
mkdir Output
maffilter param=MafFilter-AllCDS.bpp
```

We create an index of all extracted regions:
```bash
python getCDSList.py
```

Then we combine all exons into complete CDS:
```bash
mkdir Output/Combine
Rscript getCDS.R
```

Check how many genes were extracted, which have all strains:
```bash
find Output/Combine -type f | xargs grep -c '>' | grep ".fa:23" | wc -l
```
=> 6742
```bash
cd Genes/Output/Combine/
ls -l *.fa > GenesAllList_Umaydis.txt
wc -l GenesAllList_Umaydis.txt
cd ../../..
```

We then find the best matching S. reilianum outgroup sequence.
We first run a all-to-all blast:
```bash
cd SiLiX/Blast
makeblastdb -in Sreilianum_valid_prot.id.fa -dbtype prot -out DbSreilianum -logfile DbSrLog.txt
blastp -db DbSreilianum -query Ustma2_2_GeneCatalog_proteins_20171205.aa.fasta -out DbSrQuUm.txt -outfmt 6 
cd ..
```
Then we run SiLiX to get gene families, trying distinct parameter combinations:
(note: the file "MergedProt.fa" contains all proteins of U. maydis and S. reilianum):
```bash
mkdir Blast/SiLiXScan
python mergeProt.py
```
```bash
for r in 0.05 0.10 0.15 0.20 0.25 0.30 0.35 0.40 0.45 0.50 0.55 0.60 0.65 0.70 0.75 0.80 0.85 0.90 0.95 
do
   s=$(echo $r*100 | bc)
   t=${s/.*}
   for i in 0.05 0.10 0.15 0.20 0.25 0.30 0.35 0.40 0.45 0.50 0.55 0.60 0.65 0.70 0.75 0.80 0.85 0.90 0.95
   do
      j=$(echo $i*100 | bc) 
      k=${j/.*}
      echo $r   $i
      silix Blast/SiLiXScan/MergedProt.fa Blast/DbSrQuUm.txt -r $r -i $i -f FAM > Blast/SiLiXScan/seq.fnodes_r${t}i${k}
   done
done
```
Analyze SiLiX output and creates family list:
```bash
Rscript parseSiLiX.R
cd ..
```

We use the SiLiX results to add the corresponding outgroup sequences:
```bash
mkdir -p Output/Combine/AddGenes/Homologs/
python addSeq.py
```

In a second step, we deal with paralogs by further adding an outgroup sequence from the blast results.
First, read all Um sequences and creates `ExtractGenes_onlyum_v190512.txt`:
```bash
python checkCombineCDS.py
```

Collect all gene ids which are not covered by the SiLiX analysis.
Then collect the according sequences in fasta format; these files are the input for the second blastp search.
```bash
mkdir SiLiX/Blast/MissingBlast
Rscript getMissingGenes.R
python getMissingSeqs.py
```

First, an all-against-all blastp search was performed with the proteomes of U. maydis and S. reilianum. The result
was then used in SiLiX to map Sr sequences to Um gene families (extracted from the multiple genome alignment). However,
not to all Um gene families, a Sr homolog could be assigned.Therefore, I get a list of all Um genes that do not have
an assigned Sr proteins. I take this list and the list of un-assigned Sr proteins and perform a second blastp search.
Then, I assign the best single hit in Sr to the Um genes.

Create a blast database with the missing sequences from S. reilianum:
```bash
cd SiLiX/Blast/MissingBlast
makeblastdb -in MissingSrSeqs.fa -dbtype prot -out DbSrMis -logfile DbSrMisLog
```

Run the blastp search:
```bash
blastp -db DbSrMis -query MissingUmSeqs.fa -out DbSrMisQuUmMis.log -outfmt 6
cd ../../..
```

Outputs all best single blastp hits (no paralogs in either direction).
```bash
Rscript findBlastHit.R
```

Now add the resulting outgroup sequences (this script adds the S. reilianum sequence to the extracted 23 Um sequences):
```bash
mkdir Output/Combine/AddGenes/Homologs2
python addSeq2.py
```

Combine results of addSeq.py and addSeq2.py:
```bash
python combineGeneList.py
```
This creates file `Output/Combine/AddGenes/GeneListAll.txt`.

We copy files from  Output/Combine/AddGenes/Homologs and Output/Combine/AddGenes/Homologs2 into `GenesRaw`:

```bash
cp Output/Combine/AddGenes/GeneListAll.txt .
mkdir GenesRaw/
rsync -r --include='*.fa' --exclude='*' Output/Combine/AddGenes/Homologs/ GenesRaw/ 
rsync -r --include='*.fa' --exclude='*' Output/Combine/AddGenes/Homologs2/ GenesRaw/ 
Genes % ls -1 GenesRaw | wc -l # 6082 genes
```
Note: we need rsync here, as there are too many files for cp to handle.

We then align the best match to the alignment (on a computer grid):

```bash
mkdir -p SlurmLog/out
mkdir -p SlurmLog/err
mkdir GeneAlignmentsNT
mkdir GeneAlignmentsAA

sbatch slurm_macse.sh
```

Check all alignments completed:
```bash
for i in {1..6082};
do
  gene=`python getCDS.py $i`
  if [ ! -f GeneAlignmentsNT/${gene}_NT.fa ]
  then
    echo $i $gene
  fi
done
```
All alignments completed, excepted UMAG_10543_sr15138, which crashes Macse.


Extract Mexicans strains only:

```bash
mkdir GeneAlignmentsNT_mex
python getMexSubAlignments.py > MexSubAlignments.log
```

6051 genes have all Mexican strains.


Extract pop1 and pop2 strains:

```bash
mkdir GeneAlignmentsNT_ABCPQRSTUV
python getPop1SubAlignments.py > ABCPQRSTUVSubAlignments.log
mkdir GeneAlignmentsNT_DEFGIKMNO
python getPop2SubAlignments.py > DEFGIKMNOSubAlignments.log
mkdir GeneAlignmentsNT_DEFGIKNO
python getPop2bSubAlignments.py > DEFGIKNOSubAlignments.log
```


## Check secondary metabolites genes

```bash
count=0
for i in `cut -f 3 ListSMGC.csv`
do
  if [ -f "Output/Combine/$i.fa" ]
  then
    count=$((count+1))
  fi
done
echo "$count genes found."

```
All genes present!

Check how many have all mexican strains:

```bash
count=0
for i in `cut -f 3 ListSMGC.csv`
do
  if [ -f "Output/Combine/$i.fa" ]
  then
    nline=`grep '>' Output/Combine/$i.fa | wc -l`
    echo "File $i has $nline sequences."
    if [ $nline == 23 ]
    then
      count=$((count+1))
    fi
  fi
done
echo "$count complete genes."

```
All genes have the 23 sequences.

Check with outgroup:

```bash
count=0
for i in `cut -f 3 ListSMGC.csv`
do
  if stat -t GenesRaw/$i*.fa >/dev/null 2>&1
  then
    count=$((count+1))
  fi
done
echo "$count genes found."

```

Only 14 genes have an outgroup.

