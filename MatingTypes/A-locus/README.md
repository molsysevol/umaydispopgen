<!-- Created on 19/08/20 by jdutheil -->

Create a blast database for each genome individually, and look for the mating type loci.

```bash
for STRAIN in A B C D E F G H I J K L M N O P Q R S T U V
do
  echo "Making BLAST+ database for strain $STRAIN..."
  makeblastdb -in ../Assemblies/Strain$STRAIN -title "Strain${STRAIN} DB" -dbtype nucl -out Strain${STRAIN}_DB
done
```

Then we blast the mating type sequence on each strain:
```bash
```bash
for STRAIN in A B C D E F G H I J K L M N O P Q R S T U V
do
  echo "Blasting strain $STRAIN..."
  blastn -db Strain${STRAIN}_DB -query a1-locus.fasta -evalue 1e-6 -out Blast_a1_Strain${STRAIN}.txt -outfmt 6
  blastn -db Strain${STRAIN}_DB -query a2-locus.fasta -evalue 1e-6 -out Blast_a2_Strain${STRAIN}.txt -outfmt 6
done

```

We get all a1 sequences:

```r
library(seqinr)

read.locus <- function(strain, scaffold, start, stop, comp) {
  assembly <- read.fasta(paste("../../Assemblies/Strain", strain, sep = ""))
  sequence <- assembly[[scaffold]][start:stop]
  if (comp)
    sequence <- rev(comp(sequence))
  return(paste(sequence, collapse = ""))
}

a1.strains <- c("A", "B", "D", "J", "K", "L", "M", "N", "O", "Q", "R", "U")

file <- "a1-locus-allStrains.fasta"
seq.a1<-read.fasta("a1-locus.fasta")
cat(">a1\n", paste(seq.a1$U37795.1, collapse=""), "\n", file = file, append = FALSE)

for (strain in a1.strains) {
  dat <- read.table(paste("Blast_a1_Strain", strain, ".txt", sep = ""), header = FALSE, stringsAsFactors = FALSE)
  scaffold <- dat$V2[1]
  dat <- subset(dat, V2 == scaffold)
  start <- min(c(dat$V9, dat$V10))
  stop  <- max(c(dat$V9, dat$V10))
  if (all(dat$V9 > dat$V10)) {
    print("Negative strand")
    comp <- TRUE
  } else if (all(dat$V9 < dat$V10)) {
    comp <- FALSE
  } else {
    stop("ERROR!!! Ambiguous orientation.")
  }
  sequence <- read.locus(strain, scaffold, start, stop, comp)
  scaffold <- strsplit(scaffold, ":")[[1]][2]
  cat(strain, scaffold, start, stop, "\n")
  cat(paste(">Strain", strain, ".", scaffold, "\n", sequence, "\n", sep = ""), file = file, append = TRUE)
}

```


We get all a2 sequences:

```r
library(seqinr)

read.locus <- function(strain, scaffold, start, stop, comp) {
  assembly <- read.fasta(paste("../../Assemblies/Strain", strain, sep = ""))
  sequence <- assembly[[scaffold]][start:stop]
  if (comp)
    sequence <- rev(comp(sequence))
  return(paste(sequence, collapse = ""))
}

a2.strains <- c("C", "E", "F", "G", "H", "I", "P", "S", "T", "V")

file <- "a2-locus-allStrains.fasta"
seq.a2<-read.fasta("a2-locus.fasta")
cat(">a2\n", paste(seq.a2$U37796.1, collapse=""), "\n", file = file, append = FALSE)

for (strain in a2.strains) {
  dat <- read.table(paste("Blast_a2_Strain", strain, ".txt", sep = ""), header = FALSE, stringsAsFactors = FALSE)
  scaffold <- dat$V2[1]
  dat <- subset(dat, V2 == scaffold)
  start <- min(c(dat$V9, dat$V10))
  stop  <- max(c(dat$V9, dat$V10))
  if (all(dat$V9 > dat$V10)) {
    print("Negative strand")
    comp <- TRUE
  } else if (all(dat$V9 < dat$V10)) {
    comp <- FALSE
  } else {
    stop("ERROR!!! Ambiguous orientation.")
  }
  sequence <- read.locus(strain, scaffold, start, stop, comp)
  scaffold <- strsplit(scaffold, ":")[[1]][2]
  cat(strain, scaffold, start, stop, "\n")
  cat(paste(">Strain", strain, ".", scaffold, "\n", sequence, "\n", sep = ""), file = file, append = TRUE)
}

```


We make an alignment of strain A with the reference genome + the two reference a1 and a2 sequences.
We start by extracting the corresponding sequences from chr05 and scaffold79:
```r
library(seqinr)

seq.chr05<-read.fasta("Mummer/Chr05.fasta")
subseq<-seq.chr05$chr_05[1022000:1029000] #1023522-1028134
cat(">chr05\n", paste(subseq, collapse=""), "\n", file = "StrainA-locus-A.fasta")

seq.strainA<-read.fasta("Mummer/StrainA-Scaffold79.fasta")
subseq<-seq.strainA$StrainAscaffold79[75000:90000]
cat(">StrainA.scaffold79\n", paste(subseq, collapse=""), "\n", file = "StrainA-locus-A.fasta", append = TRUE)

seq.strainB<-read.fasta("Mummer/StrainB-Scaffold81.fasta")
subseq<-seq.strainB$StrainBscaffold81[361400:367900]
cat(">StrainB.scaffold81\n", paste(subseq, collapse=""), "\n", file = "StrainA-locus-A.fasta", append = TRUE)


seq.a1<-read.fasta("a1-locus.fasta")
cat(">a1\n", paste(seq.a1$U37795.1, collapse=""), "\n", file = "StrainA-locus-A.fasta", append = TRUE)

seq.a2<-read.fasta("a2-locus.fasta")
cat(">a2\n", paste(seq.a2$U37796.1, collapse=""), "\n", file = "StrainA-locus-A.fasta", append = TRUE)
```

Group1: a1 ref + chr05 + strainA + strainB
```bash
muscle -in StrainA-locus-A-group1.fasta -out StrainA-locus-A-group1-muscle.fasta
muscle -in a2-locus-allStrains+StrainA.fasta -out a2-locus-allStrains+StrainA-muscle.fasta
```
Note: the ref a2 sequence was added manually to the mexican ones before alignment.

We make a profile alignment:
```
muscle -profile -in1 StrainA-locus-A-group1-muscle.fasta -in2 a2-locus-allStrains+StrainA-muscle.fasta -out StrainA-locus-A-allSeqs-muscle.fasta
```
Muscle performs badly on tha one!
```
clustalo --p1 StrainA-locus-A-group1-muscle.fasta --p2 a2-locus-allStrains+StrainA-muscle.fasta -o StrainA-locus-A-allSeqs-clustalo.fasta
```
This looks much better.


# Look at the polymorphism in mexican strains compared to 501 strain

We convert the fast alignment into a maf file, and the genbank coordinates into a bed file, and then use maffilter to extract genes:

```bash
python3 convertAln.py a1-locus-allStrains-aln
python3 convertAln.py a2-locus-allStrains-aln
```
Then we create a1-locus.gff and a2-locus.gff by copying the CDS coordinates from the GenBank file.

```bash
mkdir a1-Genes
maffilter param=MafFilter-ExtractCDS.bpp MTL=a1 REF=a1

mkdir a2-Genes
maffilter param=MafFilter-ExtractCDS.bpp MTL=a2 REF=a2

python3 concatAln.py
```

We further extract the deleted region in Mexican a2 and save it to a2-deleted-region.fasta, using SeaView.
We then blast it using NCBI blast, and only got U. maydis a2 locus sequences (14).

We try TE annotation, using EDTA 1.9.4:
```bash
docker run -v $PWD:/in -w /in quay.io/biocontainers/edta:1.9.4--0 EDTA.pl --genome a2-deleted-region.fasta
```

