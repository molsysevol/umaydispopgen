# created by Gabriel on Feb 08, 2019
# last modified by Gabriel on Feb 08, 2019

# Here, I merge the Umaydis and Sreilianum proteome for analysis with SiLiX.

from Bio import SeqIO

umseq = open("Blast/Ustma2_2_GeneCatalog_proteins_20171205.aa.fasta")
umrec = list(SeqIO.parse(umseq, "fasta"))
	
srseq = open("Blast/Sreilianum_valid_prot.id.fa")
srrec = list(SeqIO.parse(srseq, "fasta"))
	
with open("Blast/SiLiXScan/MergedProt.fa", "w") as out_file:
	for i in range(len(umrec)):
		out_file.write(">" + umrec[i].id + "\n" + str(umrec[i].seq) + "\n")
	for j in range(len(srrec)):
		out_file.write(">" + srrec[j].id + "\n" + str(srrec[j].seq) + "\n")
