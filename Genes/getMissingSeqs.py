# created by Gabriel on Jul 03, 2019
# last modified by Gabriel on Jul 03, 2019

# Here, I read the list of missing (not mapped) genes in U. maydis
# and S. reilianum and write the corresponding sequences to a fasta file.

from Bio import SeqIO

# parse U. maydis data:
um_ids = []
for line in (open("SiLiX/Blast/MissingBlast/MissingUmGenes.txt")).readlines():
	um_ids.append(line.rstrip())

umseq = open("SiLiX/Blast/Ustma2_2_GeneCatalog_proteins_20171205.aa.fasta")
umrec = list(SeqIO.parse(umseq, "fasta"))

with open("SiLiX/Blast/MissingBlast/MissingUmSeqs.fa", "w") as out_file:
	for r in range(len(umrec)):
		x = (umrec[r].id).split("mRNA_")[1]
		if x in um_ids:
			out_file.write(">" + x + "\n" + str(umrec[r].seq) + "\n")



sr_ids = []
for line2 in (open("SiLiX/Blast/MissingBlast/MissingSrGenes.txt")).readlines():
	sr_ids.append(line2.rstrip())

srseq = open("SiLiX/Blast/Sreilianum_valid_prot.id.fa")
srrec = list(SeqIO.parse(srseq, "fasta"))

with open("SiLiX/Blast/MissingBlast/MissingSrSeqs.fa", "w") as out_file2:
	for s in range(len(srrec)):
		if srrec[s].id in sr_ids:
			out_file2.write(">" + srrec[s].id + "\n" + str(srrec[s].seq) + "\n")
