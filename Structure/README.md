# Analysis of population structure

Created on 08/01/19 by jdutheil

Pre-processing of data
======================

We filter SNPs based on LD:

First convert to BCF:
```bash
export BCFTOOLS_PLUGINS=$HOME/Programs/bcftools/plugins #To be adapted to local configuration
bcftools view -Ob -o Umaydis.snp.bcf ../Umaydis.snp.vcf.gz
```

Then filter based on LD:
```bash
bcftools +prune -i 'FILTER="PASS"' -w 250000bp -l 0.6 -Ou -o Umaydis.snp.unlinked.bcf  Umaydis.snp.bcf 
```

Convert to plink:
```bash
plink1.9 --bcf Umaydis.snp.unlinked.bcf --allow-extra-chr --chr 1-23 --make-bed --out Umaydis.snp.unlinked
```

Principal component analysis
============================

Using SmartPCA.

```bash
mkdir smartPCA #and edit par file there
awk '{print $1,$2,$3,$4,$5,1}' Umaydis.snp.unlinked.fam > smartPCA/Umaydis.PCA.fam

smartpca -p ./smartPCA/Umaydis.unlinked.par
```

* visualization of results in AnalysisPCA.R

Analysis of population structure with ADMIXTURE
===============================================

```bash
plink1.9 --bcf Umaydis.snp.unlinked.bcf --allow-extra-chr --chr 1-23 --make-bed --recode 12 --out Umaydis.snp.unlinked.recode12
```
* Admixture subdirectory

