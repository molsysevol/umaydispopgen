#! /usr/bin/python3

from Bio import AlignIO

###
### B Locus
###

# UMAG_12052 (negative strand)
aln1 = AlignIO.read("Umaydis_matingTypeLocus7-chr01-1690307-1691499.fasta", "fasta")
aln2 = AlignIO.read("Umaydis_matingTypeLocus6-chr01-1690003-1690233.fasta", "fasta")
aln1.sort()
aln2.sort()
aln12052 = aln1 + aln2
AlignIO.write(aln12052, "B_locus_UMAG_12052.fasta", "fasta")

# UMAG_UMAG_00578 (positive strand)
aln3 = AlignIO.read("Umaydis_matingTypeLocus9-chr01-1692573-1693905.fasta", "fasta")
aln4 = AlignIO.read("Umaydis_matingTypeLocus8-chr01-1691753-1692302.fasta", "fasta")
aln3.sort()
aln4.sort()
aln00578 = aln4 + aln3
AlignIO.write(aln00578, "B_locus_UMAG_00578.fasta", "fasta")

###
### A Locus
###

# UMAG_02382 (negative strand)
aln5 = AlignIO.read("Umaydis_matingTypeLocus1-chr05-1023521-1023644.fasta", "fasta")
aln5.sort()
aln02382 = aln5
AlignIO.write(aln02382, "A_locus_UMAG_02382.fasta", "fasta")

# UMAG_02383 (negative strand)
aln6 = AlignIO.read("Umaydis_matingTypeLocus2-chr05-1025468-1025586.fasta", "fasta")
aln7 = AlignIO.read("Umaydis_matingTypeLocus3-chr05-1025671-1026107.fasta", "fasta")
aln8 = AlignIO.read("Umaydis_matingTypeLocus4-chr05-1026180-1026498.fasta", "fasta")
aln9 = AlignIO.read("Umaydis_matingTypeLocus5-chr05-1026577-1026779.fasta", "fasta")
aln6.sort()
aln7.sort()
aln8.sort()
aln9.sort()
aln02383 = aln9 + aln8 + aln7 + aln6
AlignIO.write(aln02383, "A_locus_UMAG_02383.fasta", "fasta")



