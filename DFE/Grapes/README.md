# Created on 13/02/19 by jdutheil
# Run Grapes analyses.

Note: run `parsePopStatsOutput.R` to generate input files for Grapes.


Mexican strains only:
```bash
grapes -in Umaydis_mexicans_unfolded_allgenes.sfs -out Umaydis_mexicans_unfolded_AllModels_nodiv_allgenes.res -model all -no_div_param
grapes -in Umaydis_mexicans_unfolded_effectors.sfs -out Umaydis_mexicans_unfolded_AllModels_nodiv_effectors.res -model all -no_div_param
grapes -in Umaydis_mexicans_unfolded_clusters.sfs -out Umaydis_mexicans_unfolded_AllModels_nodiv_clusters.res -model all -no_div_param
grapes -in Umaydis_mexicans_unfolded_effectorsNotInClusters.sfs -out Umaydis_mexicans_unfolded_AllModels_nodiv_effectorsNotInClusters.res -model all -no_div_param
grapes -in Umaydis_mexicans_unfolded_notEffectorsNotInClusters.sfs -out Umaydis_mexicans_unfolded_AllModels_nodiv_notEffectorsNotInClusters.res -model all -no_div_param
```

Population 1:
```bash
grapes -in Umaydis_ABCPQRSTUV_unfolded_allgenes.sfs -out Umaydis_ABCPQRSTUV_unfolded_AllModels_nodiv_allgenes.res -model all -no_div_param
grapes -in Umaydis_ABCPQRSTUV_unfolded_effectors.sfs -out Umaydis_ABCPQRSTUV_unfolded_AllModels_nodiv_effectors.res -model all -no_div_param
grapes -in Umaydis_ABCPQRSTUV_unfolded_clusters.sfs -out Umaydis_ABCPQRSTUV_unfolded_AllModels_nodiv_clusters.res -model all -no_div_param
grapes -in Umaydis_ABCPQRSTUV_unfolded_effectorsNotInClusters.sfs -out Umaydis_ABCPQRSTUV_unfolded_AllModels_nodiv_effectorsNotInClusters.res -model all -no_div_param
grapes -in Umaydis_ABCPQRSTUV_unfolded_notEffectorsNotInClusters.sfs -out Umaydis_ABCPQRSTUV_unfolded_AllModels_nodiv_notEffectorsNotInClusters.res -model all -no_div_param
```

Population 2:
```bash
grapes -in Umaydis_DEFGIKMNO_unfolded_allgenes.sfs -out Umaydis_DEFGIKMNO_unfolded_AllModels_nodiv_allgenes.res -model all -no_div_param
grapes -in Umaydis_DEFGIKMNO_unfolded_effectors.sfs -out Umaydis_DEFGIKMNO_unfolded_AllModels_nodiv_effectors.res -model all -no_div_param
grapes -in Umaydis_DEFGIKMNO_unfolded_clusters.sfs -out Umaydis_DEFGIKMNO_unfolded_AllModels_nodiv_clusters.res -model all -no_div_param
grapes -in Umaydis_DEFGIKMNO_unfolded_effectorsNotInClusters.sfs -out Umaydis_DEFGIKMNO_unfolded_AllModels_nodiv_effectorsNotInClusters.res -model all -no_div_param
grapes -in Umaydis_DEFGIKMNO_unfolded_notEffectorsNotInClusters.sfs -out Umaydis_DEFGIKMNO_unfolded_AllModels_nodiv_notEffectorsNotInClusters.res -model all -no_div_param
```

Population 2 without strain M (identical to N):
```bash
grapes -in Umaydis_DEFGIKNO_unfolded_allgenes.sfs -out Umaydis_DEFGIKNO_unfolded_AllModels_nodiv_allgenes.res -model all -no_div_param
grapes -in Umaydis_DEFGIKNO_unfolded_effectors.sfs -out Umaydis_DEFGIKNO_unfolded_AllModels_nodiv_effectors.res -model all -no_div_param
grapes -in Umaydis_DEFGIKNO_unfolded_clusters.sfs -out Umaydis_DEFGIKNO_unfolded_AllModels_nodiv_clusters.res -model all -no_div_param
grapes -in Umaydis_DEFGIKNO_unfolded_effectorsNotInClusters.sfs -out Umaydis_DEFGIKNO_unfolded_AllModels_nodiv_effectorsNotInClusters.res -model all -no_div_param
grapes -in Umaydis_DEFGIKNO_unfolded_notEffectorsNotInClusters.sfs -out Umaydis_DEFGIKNO_unfolded_AllModels_nodiv_notEffectorsNotInClusters.res -model all -no_div_param
```

Compare with short genes only:
```bash
grapes -in Umaydis_mexicans_unfolded_effectorsNotInClustersSmall.sfs -out Umaydis_mexicans_unfolded_AllModels_nodiv_effectorsNotInClustersSmall.res -model all -no_div_param
grapes -in Umaydis_mexicans_unfolded_notEffectorsNotInClustersSmall.sfs -out Umaydis_mexicans_unfolded_AllModels_nodiv_notEffectorsNotInClustersSmall.res -model all -no_div_param
```

