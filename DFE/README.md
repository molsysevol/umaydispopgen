# DFE analyses

Created on 12/02/19 by jdutheil
Last Modif. 23/05/19 by jdutheil

Prepare data for DFE analyses
=============================

* runPopStats.R

All strains
-----------

```bash
grep  -r "discarded" PopStatsLogs | grep -v "discarded 1" | wc -l
```
=> 57 alignments with multiple stop codons. We retrieve them:
```bash
grep  -r "discarded" PopStatsLogs | grep -v "discarded 1" | perl -n -e'/PopStatsLogs\/(.*).log\:/ && print "$1\n"' > MultipleStopCodons_AllIndividuals.txt
```


Mexican strains only
--------------------
```bash
grep  -r "discarded" PopStatsMexicansLogs | grep -v "discarded 1" | wc -l
```
=> 56 alignments with multiple stop codons. We retrieve them:
```bash
grep  -r "discarded" PopStatsMexicansLogs | grep -v "discarded 1" | perl -n -e'/PopStatsMexicansLogs\/(.*).log\:/ && print "$1\n"' > MultipleStopCodons_Mexicans.txt
```

Per subpopulations
------------------

```bash
grep  "discarded" PopStatsABCPQRSTUVLogs/*.log | grep -v "discarded 1" | wc -l
```
=> 49 alignments with multiple stop codons. We retrieve them:
```bash
grep  -r "discarded" PopStatsABCPQRSTUVLogs | grep -v "discarded 1" | perl -n -e'/PopStatsABCPQRSTUVLogs\/(.*).log\:/ && print "$1\n"' > MultipleStopCodons_ABCPQRSTUV.txt
```


Pop 2:
```bash
grep  "discarded" PopStatsDEFGIKMNOLogs/*.log | grep -v "discarded 1" | wc -l
```
=> 42 alignments with multiple stop codons. We retrieve them:
```bash
grep  -r "discarded" PopStatsDEFGIKMNOLogs | grep -v "discarded 1" | perl -n -e'/PopStatsDEFGIKMNOLogs\/(.*).log\:/ && print "$1\n"' > MultipleStopCodons_DEFGIKMNO.txt
```

Pop2 without M:
```bash
grep  "discarded" PopStatsDEFGIKNOLogs/*.log | grep -v "discarded 1" | wc -l
```
=> 42 alignments with multiple stop codons. We retrieve them:
```bash
grep  -r "discarded" PopStatsDEFGIKNOLogs | grep -v "discarded 1" | perl -n -e'/PopStatsDEFGIKNOLogs\/(.*).log\:/ && print "$1\n"' > MultipleStopCodons_DEFGIKNO.txt
```


Grapes analyses
===============

* Grapes sub-directory

