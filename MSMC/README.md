# Multiple sequentially Markov coalescent analyses

Created on 22/10/18 by jdutheil

All mexican strains:
====================

```bash
cd Mexicans

maffilter param=MafFilter-toMSMC.bpp
```

Then split per chromosome:
```bash
awk '{f="Umaydis_mexicans_" $1 ".msmc"; print >> f; close(f)}' Umaydis_mexicans.msmc
```

And run msmc
```bash
msmc2 -o Umaydis_mexicans_msmc2 \
        Umaydis_mexicans_chr01.msmc \
        Umaydis_mexicans_chr02.msmc \
        Umaydis_mexicans_chr03.msmc \
        Umaydis_mexicans_chr04.msmc \
        Umaydis_mexicans_chr05.msmc \
        Umaydis_mexicans_chr06.msmc \
        Umaydis_mexicans_chr07.msmc \
        Umaydis_mexicans_chr08.msmc \
        Umaydis_mexicans_chr09.msmc \
        Umaydis_mexicans_chr10.msmc \
        Umaydis_mexicans_chr11.msmc \
        Umaydis_mexicans_chr12.msmc \
        Umaydis_mexicans_chr13.msmc \
        Umaydis_mexicans_chr14.msmc \
        Umaydis_mexicans_chr15.msmc \
        Umaydis_mexicans_chr16.msmc \
        Umaydis_mexicans_chr17.msmc \
        Umaydis_mexicans_chr18.msmc \
        Umaydis_mexicans_chr19.msmc \
        Umaydis_mexicans_chr20.msmc \
        Umaydis_mexicans_chr21.msmc \
        Umaydis_mexicans_chr22.msmc \
        Umaydis_mexicans_chr23.msmc
```

Strains LHJ:
============

```bash
mkdir LHJ
cd LHJ

maffilter param=MafFilter-toMSMC.bpp
```

Then split per chromosome:
```bash
awk '{f="Umaydis_lhj_" $1 ".msmc"; print >> f; close(f)}' Umaydis_lhj.msmc
```

And run msmc
```bash
msmc2 -o Umaydis_lhj_msmc2 \
        Umaydis_lhj_chr01.msmc \
        Umaydis_lhj_chr02.msmc \
        Umaydis_lhj_chr03.msmc \
        Umaydis_lhj_chr04.msmc \
        Umaydis_lhj_chr05.msmc \
        Umaydis_lhj_chr06.msmc \
        Umaydis_lhj_chr07.msmc \
        Umaydis_lhj_chr08.msmc \
        Umaydis_lhj_chr09.msmc \
        Umaydis_lhj_chr10.msmc \
        Umaydis_lhj_chr11.msmc \
        Umaydis_lhj_chr12.msmc \
        Umaydis_lhj_chr13.msmc \
        Umaydis_lhj_chr14.msmc \
        Umaydis_lhj_chr15.msmc \
        Umaydis_lhj_chr16.msmc \
        Umaydis_lhj_chr17.msmc \
        Umaydis_lhj_chr18.msmc \
        Umaydis_lhj_chr19.msmc \
        Umaydis_lhj_chr20.msmc \
        Umaydis_lhj_chr21.msmc \
        Umaydis_lhj_chr22.msmc \
        Umaydis_lhj_chr23.msmc
```

Strains DEFGIKMNO
=================

```bash
mkdir DEFGIKMNO
cd DEFGIKMNO
maffilter param=MafFilter-toMSMC.bpp
```

Then split per chromosome:
```bash
awk '{f="Umaydis_defgikmno_" $1 ".msmc"; print >> f; close(f)}' Umaydis_defgikmno.msmc
```
And run msmc
```bash
msmc2 -o Umaydis_defgikmno_msmc2 \
        Umaydis_defgikmno_chr01.msmc \
        Umaydis_defgikmno_chr02.msmc \
        Umaydis_defgikmno_chr03.msmc \
        Umaydis_defgikmno_chr04.msmc \
        Umaydis_defgikmno_chr05.msmc \
        Umaydis_defgikmno_chr06.msmc \
        Umaydis_defgikmno_chr07.msmc \
        Umaydis_defgikmno_chr08.msmc \
        Umaydis_defgikmno_chr09.msmc \
        Umaydis_defgikmno_chr10.msmc \
        Umaydis_defgikmno_chr11.msmc \
        Umaydis_defgikmno_chr12.msmc \
        Umaydis_defgikmno_chr13.msmc \
        Umaydis_defgikmno_chr14.msmc \
        Umaydis_defgikmno_chr15.msmc \
        Umaydis_defgikmno_chr16.msmc \
        Umaydis_defgikmno_chr17.msmc \
        Umaydis_defgikmno_chr18.msmc \
        Umaydis_defgikmno_chr19.msmc \
        Umaydis_defgikmno_chr20.msmc \
        Umaydis_defgikmno_chr21.msmc \
        Umaydis_defgikmno_chr22.msmc \
        Umaydis_defgikmno_chr23.msmc
```

Strains DEFGIKNO
=================

Strains M and N are almost identical, we remove one for a check:
```bash
mkdir DEFGIKNO
cd DEFGIKNO
maffilter param=MafFilter-toMSMC.bpp
```

Then split per chromosome:
```bash
awk '{f="Umaydis_defgikno_" $1 ".msmc"; print >> f; close(f)}' Umaydis_defgikno.msmc
```
And run msmc
```bash
msmc2 -o Umaydis_defgikno_msmc2 \
        Umaydis_defgikno_chr01.msmc \
        Umaydis_defgikno_chr02.msmc \
        Umaydis_defgikno_chr03.msmc \
        Umaydis_defgikno_chr04.msmc \
        Umaydis_defgikno_chr05.msmc \
        Umaydis_defgikno_chr06.msmc \
        Umaydis_defgikno_chr07.msmc \
        Umaydis_defgikno_chr08.msmc \
        Umaydis_defgikno_chr09.msmc \
        Umaydis_defgikno_chr10.msmc \
        Umaydis_defgikno_chr11.msmc \
        Umaydis_defgikno_chr12.msmc \
        Umaydis_defgikno_chr13.msmc \
        Umaydis_defgikno_chr14.msmc \
        Umaydis_defgikno_chr15.msmc \
        Umaydis_defgikno_chr16.msmc \
        Umaydis_defgikno_chr17.msmc \
        Umaydis_defgikno_chr18.msmc \
        Umaydis_defgikno_chr19.msmc \
        Umaydis_defgikno_chr20.msmc \
        Umaydis_defgikno_chr21.msmc \
        Umaydis_defgikno_chr22.msmc \
        Umaydis_defgikno_chr23.msmc
```

Strains ABCPQRSTUV
==================

```bash
mkdir ABCPQRSTUV
cd ABCPQRSTUV

maffilter param=MafFilter-toMSMC.bpp
```

Then split per chromosome:
```bash
awk '{f="Umaydis_abcpqrstuv_" $1 ".msmc"; print >> f; close(f)}' Umaydis_abcpqrstuv.msmc
```

And run msmc
```bash
msmc2 -o Umaydis_abcpqrstuv_msmc2 \
         Umaydis_abcpqrstuv_chr01.msmc \
         Umaydis_abcpqrstuv_chr02.msmc \
         Umaydis_abcpqrstuv_chr03.msmc \
         Umaydis_abcpqrstuv_chr04.msmc \
         Umaydis_abcpqrstuv_chr05.msmc \
         Umaydis_abcpqrstuv_chr06.msmc \
         Umaydis_abcpqrstuv_chr07.msmc \
         Umaydis_abcpqrstuv_chr08.msmc \
         Umaydis_abcpqrstuv_chr09.msmc \
         Umaydis_abcpqrstuv_chr10.msmc \
         Umaydis_abcpqrstuv_chr11.msmc \
         Umaydis_abcpqrstuv_chr12.msmc \
         Umaydis_abcpqrstuv_chr13.msmc \
         Umaydis_abcpqrstuv_chr14.msmc \
         Umaydis_abcpqrstuv_chr15.msmc \
         Umaydis_abcpqrstuv_chr16.msmc \
         Umaydis_abcpqrstuv_chr17.msmc \
         Umaydis_abcpqrstuv_chr18.msmc \
         Umaydis_abcpqrstuv_chr19.msmc \
         Umaydis_abcpqrstuv_chr20.msmc \
         Umaydis_abcpqrstuv_chr21.msmc \
         Umaydis_abcpqrstuv_chr22.msmc \
         Umaydis_abcpqrstuv_chr23.msmc
```

Check convergence by starting with a higher recombination rate:

```bash
msmc2 -o Umaydis_abcpqrstuv_msmc2_highRho \
      -r 10 \
         Umaydis_abcpqrstuv_chr01.msmc \
         Umaydis_abcpqrstuv_chr02.msmc \
         Umaydis_abcpqrstuv_chr03.msmc \
         Umaydis_abcpqrstuv_chr04.msmc \
         Umaydis_abcpqrstuv_chr05.msmc \
         Umaydis_abcpqrstuv_chr06.msmc \
         Umaydis_abcpqrstuv_chr07.msmc \
         Umaydis_abcpqrstuv_chr08.msmc \
         Umaydis_abcpqrstuv_chr09.msmc \
         Umaydis_abcpqrstuv_chr10.msmc \
         Umaydis_abcpqrstuv_chr11.msmc \
         Umaydis_abcpqrstuv_chr12.msmc \
         Umaydis_abcpqrstuv_chr13.msmc \
         Umaydis_abcpqrstuv_chr14.msmc \
         Umaydis_abcpqrstuv_chr15.msmc \
         Umaydis_abcpqrstuv_chr16.msmc \
         Umaydis_abcpqrstuv_chr17.msmc \
         Umaydis_abcpqrstuv_chr18.msmc \
         Umaydis_abcpqrstuv_chr19.msmc \
         Umaydis_abcpqrstuv_chr20.msmc \
         Umaydis_abcpqrstuv_chr21.msmc \
         Umaydis_abcpqrstuv_chr22.msmc \
         Umaydis_abcpqrstuv_chr23.msmc
```

Cross-coalescence rate:
=======================

## ABCPQRSTUV vs. DEFGIKMNO

```bash
mkdir CrossRates
cd CrossRates

maffilter param=MafFilter-toMSMC.bpp
```
Then split per chromosome:
```bash
awk '{f="Umaydis_all_" $1 ".msmc"; print >> f; close(f)}' Umaydis_all.msmc
```

Get between pop pairs:
```r
pop1 <- 0:9
pop2 <- 10:18
repeat {
  cat(".")
  x1 <- apply(cbind(sample(pop1, replace = F),sample(pop2, replace = F)),1,paste,collapse="-")
  x2 <- apply(cbind(sample(pop1, replace = F),sample(pop2, replace = F)),1,paste,collapse="-")
 if(!any(x1 %in% x2)) break
}

paste(c(x1, x2), collapse = ",")
```

Run MSMC2:
```bash
msmc2 -o Umaydis_crossall_msmc2 \
      -I 8-12,4-10,2-16,7-18,9-14,6-13,0-17,1-11,5-15,3-12,6-15,3-14,5-12,2-18,8-17,0-10,9-16,7-11,1-13,4-15 \
         Umaydis_all_chr01.msmc \
         Umaydis_all_chr02.msmc \
         Umaydis_all_chr03.msmc \
         Umaydis_all_chr04.msmc \
         Umaydis_all_chr05.msmc \
         Umaydis_all_chr06.msmc \
         Umaydis_all_chr07.msmc \
         Umaydis_all_chr08.msmc \
         Umaydis_all_chr09.msmc \
         Umaydis_all_chr10.msmc \
         Umaydis_all_chr11.msmc \
         Umaydis_all_chr12.msmc \
         Umaydis_all_chr13.msmc \
         Umaydis_all_chr14.msmc \
         Umaydis_all_chr15.msmc \
         Umaydis_all_chr16.msmc \
         Umaydis_all_chr17.msmc \
         Umaydis_all_chr18.msmc \
         Umaydis_all_chr19.msmc \
         Umaydis_all_chr20.msmc \
         Umaydis_all_chr21.msmc \
         Umaydis_all_chr22.msmc \
         Umaydis_all_chr23.msmc 
```

Combine all results using a python script provided with MSMC2:
```bash
python3 combineCrossCoal.py Umaydis_crossall_msmc2.final.txt \
        ../ABCPQRSTUV/Umaydis_abcpqrstuv_msmc2.final.txt \
        ../DEFGIKMNO/Umaydis_defgikmno_msmc2.final.txt > Umaydis_combined.final.txt
```

## Compare with random splits

* Permutations sub-directory, 'Permutate.R' script.

