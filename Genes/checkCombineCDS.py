# created by Gabriel on Jul 19, 2018
# last modified by Gabriel on Jul 19, 2018
# last modified by Gabriel on Jan 18, 2019: change input/output paths to version Nov2018
# last modified by Gabriel on May 14, 2019: change input/output paths to version May2019

# Here, I check if all combined CDS sequences start/end with start/stop codon.

import os
from Bio import SeqIO

ref_seq = open("Ustma2_2_GeneCatalog_CDS_20171205.fasta")
ref_rec = list(SeqIO.parse(ref_seq, "fasta"))

d = "Output/Combine/"

allid = []
vl = []
ivl = []
gap_ref = []
cm = 0
for f in os.listdir(d):
    if f.startswith("UMAG_") and f.endswith(".fa"):
        if f.split(".fa")[0] not in allid:
            allid.append(f.split(".fa")[0])
        seqs = open(d + f)
        recs = list(SeqIO.parse(seqs, "fasta"))
        # if float(len(recs))/23 != len(recs)/23:
		# 	print f # did not happen (May 14, 2019)
        for r in range(len(ref_rec)):
            if (ref_rec[r].id).split("mRNA_")[1] == f.split(".")[0]:
                for k in range(len(recs)):
                    if recs[k].id == "Umaydis_ref":
                        if str(ref_rec[r].seq) == (str(recs[k].seq)).replace("-", ""):
                            vl.append(f.split(".")[0])
                        else:
                            ivl.append(f.split(".")[0])
# len(allid) 4958
# len(vl)    4958


for e in allid:
    if e not in (vl + ivl):
        print(e) # did not happen (March 17,  2020)

with open(d + "ExtractGenes_onlyum_v200317.txt", "w") as out_file:
    for v in vl:
        out_file.write(v + "\n")
