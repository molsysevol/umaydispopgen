# A closer look at diveristy along the genome
Created on 16/01/19 by jdutheil

Compute nucleotide diversity in each subpop, as well as Fst:
```bash
maffilter param=MafFilter-DiversitySubPops.bpp
```

With 2kb windows:
```bash
maffilter param=MafFilter-DiversitySubPops.bpp WSIZE=2000 LABEL=2kb
```

After removing genes (need to generate Genes.bedgraph from the R script first):
```bash
maffilter param=MafFilter-DiversitySubPopsNoGene.bpp WSIZE=2000 LABEL=2kb
```
=> Not enough data

Full intergenic regions:
```bash
maffilter param=MafFilter-DiversitySubPopsIntergenic.bpp
```



Compute divergence with outgroup S. reilianum:
```bash
maffilter param=MafFilter-DiversitySubPopsWithDiv.bpp
```

Assess the significance of Fst by randomizing individuals in populations:
```r
t <- strsplit("ABCDEFGIKMNOPQRSTUV", split = "")[[1]]
t <- paste("Strain", t, sep = "")
rep1 <- sample(t)
paste(rep1[1:10], collapse=",")
paste(rep1[11:19], collapse=",")
```

```bash
maffilter param=MafFilter-RandomizeFst.bpp DATA=Randomization1 \
                "GROUP1=StrainN,StrainV,StrainO,StrainG,StrainI,StrainC,StrainP,StrainU,StrainT,StrainR" \
                "GROUP2=StrainK,StrainE,StrainQ,StrainA,StrainM,StrainS,StrainB,StrainF,StrainD"
maffilter param=MafFilter-RandomizeFst.bpp DATA=Randomization2 \
                "GROUP1=StrainK,StrainD,StrainU,StrainG,StrainF,StrainE,StrainM,StrainI,StrainP,StrainB" \
                "GROUP2=StrainT,StrainO,StrainN,StrainV,StrainS,StrainA,StrainQ,StrainC,StrainR"
maffilter param=MafFilter-RandomizeFst.bpp DATA=Randomization3 \
                "GROUP1=StrainG,StrainC,StrainN,StrainB,StrainO,StrainV,StrainR,StrainU,StrainK,StrainA" \
                "GROUP2=StrainI,StrainP,StrainF,StrainE,StrainD,StrainS,StrainM,StrainT,StrainQ"
```

