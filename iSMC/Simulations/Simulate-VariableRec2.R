# Created on 30/04/20 by jdutheil

# Use SCRM to simulate data

library(scrm)

prefix <- "Pop2"

u <- 5.23e-9

N0 <- 1e5

Nanc <- 1e5/N0
N1 <- 5e3/N0
N2 <- 1e6/N0

T1 <- 5e3/(4*N0)
T2 <- 5e2/(4*N0)

r1 <- -log(Nanc / N1) / (T1 - T2)
r2 <- -log(N1 / N2) / (T2)

# Read the recombination map:
recmap <- read.table("../ABCPQRSTUV.recmap.txt", header = TRUE, stringsAsFactors = FALSE)
recmap$ChrCode <- as.numeric(substring(recmap$chrom, 4))

cumul.start <- function(d) {
  require(dplyr)
  d <- subset(d, !is.na(ChrCode))
  d2 <- d %>%
    group_by(ChrCode) %>%
    dplyr::summarise(chr_len = max(chromEnd) - min(chromStart)) %>%
    mutate(tot = cumsum(chr_len) - chr_len) %>%
    select(-chr_len) %>%
    left_join(d, ., by = c("ChrCode" = "ChrCode")) %>%
    arrange(ChrCode, chromStart) %>%
    mutate(cum_start = chromStart + tot)
  return(d2)
}

recmap <- cumul.start(recmap)

L <- sum(recmap$WindowSize)

rec <- recmap$Rec
pos <- recmap$cum_start
rec <- rec / 1e8 # Per position
rec <- rec * L # For the full region
rec <- rec * 4 * N0 # Scaling

# Demography:
param_demo <- paste("-n 1", N2, "-G", r2, "-eG", T2, r1, "-eN", T1, Nanc, sep = " ") 
print(param_demo)

# Recombination landscape:
param_rec <- paste("-r", rec[1], L, sep = " ")
param_rec <- paste(param_rec, paste(paste("-sr", pos[-1], rec[-1], sep = " "), collapse = " "), sep = " ")

# Mutation:
param_mut <- paste("-t", 4*u*N0*L, sep = " ")

# Run scrm (10 individuals, 10 replicates):
scrm_cmd <- paste("10 10 -SC abs", param_mut, param_demo, param_rec, sep = " ")
sim <- scrm(scrm_cmd, file = "SimulationsPop2_scrm.out")

# Create fasta files:

strains <- c("StrainA", "StrainB", "StrainC", "StrainP", "StrainQ", "StrainR", "StrainS", "StrainT", "StrainU", "StrainV")
dir.create(prefix)
for (k in 1:10) {
  snps <- sim$seg_sites[[k]]
  pos <- round(as.numeric(colnames(snps)))

  # If positions are not unique, we keep the first one only:
  filter <- rep(TRUE, length(pos))
  for (i in 2:length(pos)) {
    if (pos[i] == pos[i - 1]) 
      filter[i] <- FALSE  
  }
  table(filter)
  length(pos) - length(unique(pos)) #check
  snps <- snps[,filter]
  pos <- round(as.numeric(colnames(snps)))
  colnames(snps) <- pos
  length(pos) - length(unique(pos)) #this should now be 0

  # Check theta:
  cat("Theta for replicate", k, ":", (length(pos)/sum(1/(1:9)))/L, "\n")

  # We create an alignment:
  output <- paste(prefix, "/Simulation", k, ".fasta", sep = "")
  unlink(output)
  for (i in 1:10) {
    seq <- rep("A", L)
    x <- snps[i,]
    j <- as.numeric(names(x[x == 1]))
    seq[j] <- "T"
    cat(">", strains[i], "\n", sep = "", file = output, append = TRUE)
    cat(paste(seq, collapse = ""), "\n", sep = "", file = output, append = TRUE)
  }
}
