# created by Gabriel on Jul 05, 2019
# last modified by Gabriel on Jul 05, 2019

# Here, I combine the two gene lists (based on the genes that have 
# a mapped ortholog from the SiLiX result or from the single blastp hit).

with open("Output/Combine/AddGenes/GeneListAll.txt", "w") as out_file:
	for l1 in (open("Output/Combine/AddGenes/Homologs/GeneList.txt")).readlines():
		out_file.write(l1)
	for l2 in (open("Output/Combine/AddGenes/Homologs2/GeneList2.txt")).readlines():
		out_file.write(l2)
