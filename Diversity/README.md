# Created on 17/01/18 by jdutheil
# Diversity patterns

Run MafFilter:
```bash
maffilter param=MafFilter-Diversity.bpp
```

Look at diversity along the genome:
```r
d <- read.table("Umaydis.statistics10kb.txt", header = TRUE)
hist(d$SequenceDiversityStatistics.TajimaD)
summary(d)
```

Global theta = 0.0013345 (one outlier)
Tajima's D globally negative (-1)

# Only consider mexican strains:

```bash
maffilter param=MafFilter-Diversity2.bpp
```

```r
d <- read.table("Umaydis.mexicans.statistics10kb.txt", header = TRUE)
hist(d$SequenceDiversityStatistics.TajimaD)
summary(d)
```

Global theta = 0.0009788
Tajima's D becomes less negative (-0.2861)


# With 2kb windows, in order to test the impact of distance to genes:

```bash
maffilter param=MafFilter-Diversity2.bpp WSIZE=2000 LABEL=2kb
```



# Global tree:

```bash
maffilter param=MafFilter-GlobalPairwiseSimilarity.bpp
```

Simple fastME tree can be done in R (buildTree.R).
Otherwise, a tree (with bootstrap) can be done from the command line:
```bash
fastme -i Umaydis-concat.ph -d -g -n -s -b 1000 -B Umaydis-concat.fastme.boot.dnd #(takes ~ 1day)
```

Note: we root the fastme file using midpoint rooting in phyview. Then we edit the label names by hand.



# Check the mating type loci

See MatingTypes sub-directory.

