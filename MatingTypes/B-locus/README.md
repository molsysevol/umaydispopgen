<!-- Created on 20.08.20 by jdutheil -->

Add the know B alleles to our set and build a phylogeny.

```bash
cat bW_Alleles_nucl.fa > bW_all.fasta
cat ../../Diversity/MatingTypes/B_locus_UMAG_00578.fasta >> bW_all.fasta
cat bE_Alleles_nucl.fa > bE_all.fasta
cat ../../Diversity/MatingTypes/B_locus_UMAG_12052.fasta >> bE_all.fasta

``

In seaview: Gblocks with 3 permissive options. Translation to proteins and PhyML tree with LG09 model, Best of NNI+SPR.
For bE, small blocks in 3' regions were manually unselected.

See PlotTrees-Blocus.R for plotting the resulting trees.


