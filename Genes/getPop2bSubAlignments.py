#! /usr/bin/python
# Extract sub pop sequences

from Bio import SeqIO
from os import listdir
from os.path import join

files = [f for f in listdir("GeneAlignmentsNT") if f.endswith(".fa")]
files = sorted(files)

seqs = ['StrainD', 'StrainE',
        'StrainF', 'StrainG', 'StrainI',
        'StrainK', 'StrainN', 'StrainO',
        'Sreilianum'] 

for file in files:
  aln = SeqIO.to_dict(SeqIO.parse(join("GeneAlignmentsNT/", file), "fasta"))
  #aln.sort()
  # First test if all strains are there:
  test = True
  for seqid in seqs :
    if not seqid in aln :
      print("Error! Alignment %s is missing strain %s and will be discarded." % (file, seqid))
      test = False
  if test :
    aln2 = [ aln[seqid] for seqid in seqs ]
    SeqIO.write(aln2, join("GeneAlignmentsNT_DEFGIKNO/", file), "fasta")


