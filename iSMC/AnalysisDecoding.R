# Created on 13/01/19 by jdutheil

library(plyr)

#### Load data ####

# Get the starting positions of each chromosome alignment:
stats <- read.table("Umaydis.statistics_ismc.txt", header = TRUE, stringsAsFactors = FALSE)

read.mapper <- function(prefix, wsize, threshold, pop, index = stats) {
  map <- read.table(paste(prefix, ".rho.", wsize, ".bedgraph", sep = ""), header = TRUE, stringsAsFactors = FALSE)
  map$ChrCode <- as.numeric(substring(map$chrom, 4))
  map$Chr <- paste("chr", formatC(map$ChrCode, width = 2, flag = "0"), sep = "")
  #add +1 to last window of each chr
  map <- ddply(map, .variables = "Chr", function(d) {
    d[which.max(d$chromEnd), "chromEnd"] <- d[which.max(d$chromEnd), "chromEnd"] + 1
    return(d)
  })
  map$Population <- pop
  mis <- read.table(paste(prefix, ".missing.prop.", wsize, ".bedgraph", sep = ""), header = TRUE, stringsAsFactors = FALSE)
  filter <- apply(mis[,-c(1,2,3)], 1, mean)
  map$MeanPropMissing <- filter
  map2 <- ddply(.data = map, .variables = "chrom", .fun = function(d) {
    chr <- unique(d$Chr)
    offset <- index[index$Chr == chr, "Start"]
    d$chromStart <- d$chromStart + offset
    d$chromEnd <- d$chromEnd + offset
    return(d)
  })
  return(map2[filter <= threshold,])
}


map1.w10k <- read.mapper("ResultsDecoding/ABCPQRSTUV/Gamma/TwoKnots/Umaydis_ABCPQRSTUV", "10kb", 0.3, "ABCPQRSTUV")
map2.w10k <- read.mapper("ResultsDecoding/DEFGIKNO/Gamma/TwoKnots/Umaydis_DEFGIKNO", "10kb", 0.3, "DEFGIKNO")
map.w10k <- merge(map1.w10k, map2.w10k, by = c("chrom", "chromStart", "chromEnd", "Chr", "ChrCode"))
plot(sample_mean.x~sample_mean.y, map.w10k, log = "xy")
cor.test(~sample_mean.x+sample_mean.y, map.w10k, method = "kendall")

map1.w50k <- read.mapper("ResultsDecoding/ABCPQRSTUV/Gamma/TwoKnots/Umaydis_ABCPQRSTUV", "50kb", 0.3, "ABCPQRSTUV")
map2.w50k <- read.mapper("ResultsDecoding/DEFGIKNO/Gamma/TwoKnots/Umaydis_DEFGIKNO", "50kb", 0.3, "DEFGIKNO")
map.w50k <- merge(map1.w50k, map2.w50k, by = c("chrom", "chromStart", "chromEnd", "Chr", "ChrCode"))
plot(sample_mean.x~sample_mean.y, map.w50k, log = "xy")
cor.test(~sample_mean.x+sample_mean.y, map.w50k, method = "kendall")

map1.w100k <- read.mapper("ResultsDecoding/ABCPQRSTUV/Gamma/TwoKnots/Umaydis_ABCPQRSTUV", "100kb", 0.3, "ABCPQRSTUV")
map2.w100k <- read.mapper("ResultsDecoding/DEFGIKNO/Gamma/TwoKnots/Umaydis_DEFGIKNO", "100kb", 0.3, "DEFGIKNO")
map.w100k <- merge(map1.w100k, map2.w100k, by = c("chrom", "chromStart", "chromEnd", "Chr", "ChrCode"))
plot(sample_mean.x~sample_mean.y, map.w100k, log = "xy")
cor.test(~sample_mean.x+sample_mean.y, map.w100k, method = "kendall")

map1.chr <- ddply(map1.w10k, .variables = "ChrCode", summarize, MeanRho = mean(sample_mean), Size = max(chromEnd), Population = "ABCPQRSTUV")
map2.chr <- ddply(map2.w10k, .variables = "ChrCode", summarize, MeanRho = mean(sample_mean), Size = max(chromEnd), Population = "DEFGIKNO")
map.chr <- rbind(map1.chr, map2.chr)

#### Chromosome size ####

library(ggplot2)
p.chrsize <- ggplot(map.chr, aes(x = Size/1000, y = MeanRho)) + geom_point(aes(col = Population)) +
  geom_smooth(method = "lm", aes(col = Population)) +
  scale_color_brewer(type = "qual", palette = 6) +
  scale_x_log10() +
  xlab("Chromosome size (kb)") +
  ylab(expression(rho)) +
  theme_bw() + theme(legend.position = "top")
p.chrsize

cor.test(~Size+MeanRho, map.chr, subset=Population=="ABCPQRSTUV", method = "kendall")
# -0.37 (p = 0.01388)
cor.test(~Size+MeanRho, map.chr, subset=Population=="DEFGIKNO", method = "kendall")
# -0.21 (p = 0.1715)

df.chr <- data.frame(Pop1 = map1.chr$MeanRho, Pop2 = map2.chr$MeanRho, ChrCode = map1.chr$ChrCode)
p.chr <- ggplot(subset(df.chr, !is.na(ChrCode)), aes(x = Pop2, y = Pop1)) + geom_point() +
  geom_smooth(method = "lm") +
  scale_color_brewer(type = "qual", palette = 6) +
  xlab(expression(rho~Pop[2])) +
  ylab(expression(rho~Pop[1])) +
  theme_bw()
p.chr


library(cowplot)
p <- plot_grid(p.chr, p.chrsize, labels = "AUTO", align = "h", axis = "t")
p
ggsave(p, filename = "ChrMaps.pdf", width = 10, height = 5)



#### Along the genome: Manhattan plots ####

# Create Manhattan plots:
add.manhattan <- function(d) {
  d$Position <- (d$chromStart + d$chromEnd) / 2
  require(dplyr)
  d <- subset(d, !is.na(ChrCode))
  d2 <- d %>%
    group_by(ChrCode) %>%
    summarise(chr_len = max(chromEnd)) %>%
    mutate(tot = cumsum(chr_len) - chr_len) %>%
    select(-chr_len) %>%
    left_join(d, ., by = c("ChrCode" = "ChrCode")) %>%
    arrange(ChrCode, Position) %>%
    mutate(CumPosition = Position + tot)
  return(d2)
}

map1.w10k <- add.manhattan(map1.w10k)
map2.w10k <- add.manhattan(map2.w10k)
map1.w10k$Pop <- "ABCPQRSTUV"
map2.w10k$Pop <- "DEFGIKNO"
f <- c("Chr", "ChrCode", "Position", "CumPosition", "Pop", "sample_mean")
map.w10k <- rbind(map1.w10k[,f], map2.w10k[,f])

axisdf.w10k <- map1.w10k %>% group_by(ChrCode) %>% summarize(center = (max(CumPosition) + min(CumPosition)) / 2)

map1.w50k <- add.manhattan(map1.w50k)
map2.w50k <- add.manhattan(map2.w50k)
map1.w50k$Pop <- "ABCPQRSTUV"
map2.w50k$Pop <- "DEFGIKNO"
f <- c("Chr", "ChrCode", "Position", "CumPosition", "Pop", "sample_mean")
map.w50k <- rbind(map1.w50k[,f], map2.w50k[,f])

axisdf.w50k <- map1.w50k %>% group_by(ChrCode) %>% summarize(center = (max(CumPosition) + min(CumPosition)) / 2)

map1.w100k <- add.manhattan(map1.w100k)
map2.w100k <- add.manhattan(map2.w100k)
map1.w100k$Pop <- "ABCPQRSTUV"
map2.w100k$Pop <- "DEFGIKNO"
f <- c("Chr", "ChrCode", "Position", "CumPosition", "Pop", "sample_mean")
map.w100k <- rbind(map1.w100k[,f], map2.w100k[,f])

axisdf.w100k <- map1.w100k %>% group_by(ChrCode) %>% summarize(center = (max(CumPosition) + min(CumPosition)) / 2)

# Some annotations:
library(plyr)

x <-ddply(map1.w10k, .variables = "ChrCode", summarize, Total = max(chromEnd))
index <- c(0, cumsum(x$Total))

# Mating type locus:
dat.mt <- data.frame(Chr = c(1,1,5,5,5),
                     Start = c(1690004,1691754,1023522,1025469,1027492),
                     Stop = c(1691499,1693905,1023644,1026779,1028134),
                     Type = c("bE1", "bW1", "A1P", "A1R", "Bba1"))
dat.mt$Start <- dat.mt$Start + index[dat.mt$Chr]
dat.mt$Stop <- dat.mt$Stop + index[dat.mt$Chr]

# Virulence clusters:
## We get data from Dutheil et al, 2016 (S. scitamineum genome paper, Table S3)
clusters <- read.csv("../TableS3-UmaydisOnly.csv", stringsAsFactors = FALSE)
clusters$all.genes <- paste(clusters$gene, clusters$additional.genes, sep = ", ")
## Update gene names:
clusters$all.genes <- gsub(clusters$all.genes, pattern = "um", replacement = "UMAG_")

## Get gene annotations
genes <- read.table("../Umaydis_JGI_modJYD.gff3", stringsAsFactors = FALSE)
cds <- subset(genes, V3 == "CDS")
l1 <- strsplit(cds$V9, split = "=")
l2 <- lapply(l1, nth, n = 2)
cds$id <- unlist(l2)

for (i in 1:nrow(clusters)) {
  glst <- strsplit(clusters[i, "all.genes"], split = ", ")[[1]]
  tmp <- subset(cds, id %in% glst)
  r <- range(c(tmp$V4, tmp$V5))
  clusters[i, "Start"] <- r[1]
  clusters[i, "Stop"] <- r[2]
}
clusters$ChrNum <- as.numeric(substring(clusters$chromosome, 4))
dat.cl <- clusters[,c("ChrNum", "Start", "Stop")]
dat.cl$Start <- dat.cl$Start + index[dat.cl$Chr]
dat.cl$Stop <- dat.cl$Stop + index[dat.cl$Chr]

# Plot
p.rec <- ggplot() +
  geom_rect(data = dat.mt, aes(xmin = Start, xmax = Stop, ymin = 0.0, ymax = 0.3), fill = "green4", colour = "green4", alpha = 0.5) +
  geom_rect(data = dat.cl, aes(xmin = Start, xmax = Stop, ymin = 0.0, ymax = 0.3), fill = "orange", colour = "orange", alpha = 0.5) +
  #geom_col(data = map.w10k, aes(x = CumPosition, y = sample_mean, col = as.factor(ChrCode), fill = as.factor(ChrCode)), width = 1) +
  geom_point(data = map.w10k, aes(x = CumPosition, y = sample_mean, col = as.factor(ChrCode))) +
  scale_color_manual(values = rep(c("grey", "skyblue"), 23)) +
  scale_fill_manual(values = rep(c("grey", "skyblue"), 23)) +
  ylab(expression(rho)) + xlab("Chromosome") +
  scale_x_continuous(expand = c(0,0), label = axisdf.w10k$ChrCode, breaks = axisdf.w10k$center) +
  facet_grid(Pop~.) +
  theme_bw() +
  theme(legend.position = "none",
        panel.grid.major.x = element_blank(),
        panel.grid.minor.x = element_blank())
p.rec
p.rec.10kb <- p.rec
ggsave(p.rec, filename = "10kMaps.pdf", width = 10, height = 5)

p.rec <- ggplot() +
  geom_rect(data = dat.mt, aes(xmin = Start, xmax = Stop, ymin = 0.0, ymax = 0.25), fill = "green4", colour = "green4", alpha = 0.5) +
  geom_rect(data = dat.cl, aes(xmin = Start, xmax = Stop, ymin = 0.0, ymax = 0.25), fill = "orange", colour = "orange", alpha = 0.5) +
  #geom_col(data = map.w50k, aes(x = CumPosition, y = sample_mean, col = as.factor(ChrCode), fill = as.factor(ChrCode)), width = 1) +
  geom_point(data = map.w50k, aes(x = CumPosition, y = sample_mean, col = as.factor(ChrCode))) +
  scale_color_manual(values = rep(c("grey", "skyblue"), 23)) +
  scale_fill_manual(values = rep(c("grey", "skyblue"), 23)) +
  ylab(expression(rho)) + xlab("Chromosome") +
  scale_x_continuous(expand = c(0,0), label = axisdf.w10k$ChrCode, breaks = axisdf.w10k$center) +
  scale_y_sqrt() +
  facet_grid(Pop~.) +
  theme_bw() +
  theme(legend.position = "none",
        panel.grid.major.x = element_blank(),
        panel.grid.minor.x = element_blank())
p.rec
ggsave(p.rec, filename = "50kMaps.pdf", width = 10, height = 5)

p.rec <- ggplot() +
  geom_rect(data = dat.mt, aes(xmin = Start, xmax = Stop, ymin = 0.0, ymax = 0.15), fill = "green4", colour = "green4", alpha = 0.5) +
  geom_rect(data = dat.cl, aes(xmin = Start, xmax = Stop, ymin = 0.0, ymax = 0.15), fill = "orange", colour = "orange", alpha = 0.5) +
  #geom_col(data = map.w100k, aes(x = CumPosition, y = sample_mean, col = as.factor(ChrCode), fill = as.factor(ChrCode)), width = 1) +
  geom_point(data = map.w100k, aes(x = CumPosition, y = sample_mean, col = as.factor(ChrCode))) +
  scale_color_manual(values = rep(c("grey", "skyblue"), 23)) +
  scale_fill_manual(values = rep(c("grey", "skyblue"), 23)) +
  ylab(expression(rho)) + xlab("Chromosome") +
  scale_x_continuous(expand = c(0,0), label = axisdf.w100k$ChrCode, breaks = axisdf.w100k$center) +
  #scale_y_log10() +
  facet_grid(Pop~.) +
  theme_bw() +
  theme(legend.position = "none",
        panel.grid.major.x = element_blank(),
        panel.grid.minor.x = element_blank())
p.rec
ggsave(p.rec, filename = "100kMaps.pdf", width = 10, height = 5)

# Combined figure:
library(cowplot)
p1 <- plot_grid(p.chr, p.chrsize, labels = "AUTO", align = "h", axis = "t")
p2 <- plot_grid(p1, p.rec.10kb, labels = c("", "C"), nrow = 2)
p2
ggsave(p2, filename = "Recombination.pdf", width = 10, height = 8)

#### Tests about recombination rate in some regions ####

# Is recombination higher close to clusters?
library(GenomicRanges)

is.in.cluster <- function(dat) {
  ranges.clusters <- makeGRangesFromDataFrame(clusters)
  ranges <- makeGRangesFromDataFrame(dat, keep.extra.columns = TRUE,
                                     seqnames.field = "Chr",
                                     start.field = "chromStart",
                                     end.field = "chromEnd")
  query <- as.data.frame(findOverlaps(ranges.clusters, ranges))
  dat$InCluster <- FALSE
  dat$InCluster[query$subjectHits] <- TRUE
  return(dat)
}
map1.w10k  <- is.in.cluster(map1.w10k)
map2.w10k  <- is.in.cluster(map2.w10k)
map12.w10k <- map1.w10k[,c("sample_mean", "InCluster")]
map12.w10k$sample_mean <- (map1.w10k$sample_mean*10 + map2.w10k$sample_mean*8) / 18

boxplot(sample_mean~InCluster, map1.w10k)
lapply(with(map1.w10k, split(sample_mean, InCluster)), median)
wilcox.test(sample_mean~InCluster, map1.w10k)

boxplot(sample_mean~InCluster, map2.w10k)
lapply(with(map2.w10k, split(sample_mean, InCluster)), median)
wilcox.test(sample_mean~InCluster, map2.w10k)

boxplot(sample_mean~InCluster, map12.w10k)
wilcox.test(sample_mean~InCluster, map12.w10k)


map1.w100k  <- is.in.cluster(map1.w100k)
map2.w100k  <- is.in.cluster(map2.w100k)

boxplot(sample_mean~InCluster, map1.w100k)
wilcox.test(sample_mean~InCluster, map1.w100k)

boxplot(sample_mean~InCluster, map2.w100k)
wilcox.test(sample_mean~InCluster, map2.w100k)


#### A closer look at the mating type loci ####

# Test after masking the mating type regions:
map1.w10k.mtmasked <- read.mapper("ResultsDecodingMtMasked/ABCPQRSTUV/Gamma/TwoKnots/Umaydis_ABCPQRSTUV", "10kb", 0.3, "ABCPQRSTUV")

map1.comp <- merge(map1.w10k[,c("chrom", "chromStart", "chromEnd", "sample_mean")], map1.w10k.mtmasked[,c("chrom", "chromStart", "chromEnd", "sample_mean")], by = c("chrom", "chromStart", "chromEnd"))
plot(sample_mean.y~sample_mean.x, map1.comp); abline(0, 1, col = "red")
# Highly correlated


map1.w10k.mtmasked <- add.manhattan(map1.w10k.mtmasked)
map1.w10k.mtmasked$Pop <- "ABCPQRSTUV"
f <- c("Chr", "ChrCode", "Position", "CumPosition", "Pop", "sample_mean")
map.w10k.mtmasked <- map1.w10k.mtmasked[,f]
axisdf.w10k.mtmasked <- map1.w10k.mtmasked %>% group_by(ChrCode) %>% summarize(center = (max(CumPosition) + min(CumPosition)) / 2)

p.rec.mtmasked <- ggplot() +
  geom_rect(data = dat.mt, aes(xmin = Start, xmax = Stop, ymin = 0.0, ymax = 0.3), fill = "green4", colour = "green4", alpha = 0.5) +
  geom_rect(data = dat.cl, aes(xmin = Start, xmax = Stop, ymin = 0.0, ymax = 0.3), fill = "orange", colour = "orange", alpha = 0.5) +
  #geom_col(data = map.w10k.mtmasked, aes(x = CumPosition, y = sample_mean, col = as.factor(ChrCode), fill = as.factor(ChrCode)), width = 1) +
  geom_point(data = map.w10k.mtmasked, aes(x = CumPosition, y = sample_mean, col = as.factor(ChrCode))) +
  scale_color_manual(values = rep(c("grey", "skyblue"), 23)) +
  scale_fill_manual(values = rep(c("grey", "skyblue"), 23)) +
  ylab(expression(rho)) + xlab("Chromosome") +
  scale_x_continuous(expand = c(0,0), label = axisdf.w10k.mtmasked$ChrCode, breaks = axisdf.w10k.mtmasked$center) +
  facet_grid(Pop~.) +
  theme_bw() +
  theme(legend.position = "none",
        panel.grid.major.x = element_blank(),
        panel.grid.minor.x = element_blank())

p.rec.mtmasked
ggsave(p.rec.mtmasked, filename = "RecombinationLandscapeMatingTypeMasked.pdf", width = 10, height = 4)

# Detailed plot around mating type:

map1.w1k <- read.mapper("ResultsDecoding/ABCPQRSTUV/Gamma/TwoKnots/Umaydis_ABCPQRSTUV", "1kb", 0.1, "ABCPQRSTUV")

#We do not use the Manhattan transformed coordinates here:
dat.mt <- data.frame(Chr = c(1,1,5,5,5),
                     Start = c(1690004,1691754,1023522,1025469,1027492),
                     Stop = c(1691499,1693905,1023644,1026779,1028134),
                     Type = c("bE1", "bW1", "A1P", "A1R", "Bba1"))

p.mtb <- ggplot() +
  geom_rect(data = subset(dat.mt, Chr == 1), aes(xmin=Start*1e-3, xmax=Stop*1e-3, ymin=0, ymax=0.35), fill = "darkgreen", alpha = 0.5) +
  geom_point(data = subset(map1.w1k, chrom == "chr1" & chromStart > 1.6e6 & chromEnd < 1.75e6), aes(y=sample_mean, x=(chromStart+chromEnd)*1e-3/2)) +
  xlab("Position on chromosome 1 (kb)") + ylab(expression(rho)) + ggtitle("b locus")
p.mtb

p.mta <- ggplot() +
  geom_rect(data = subset(dat.mt, Chr == 5), aes(xmin=Start*1e-3, xmax=Stop*1e-3, ymin=0, ymax=0.35), fill = "darkgreen", alpha=0.5) +
  geom_point(data = subset(map1.w1k, chrom == "chr5" & chromStart > 0.95e6 & chromEnd < 1.1e6), aes(y=sample_mean, x=(chromStart+chromEnd)*1e-3/2)) +
  xlab("Position on chromosome 5 (kb)") + ylab(expression(rho)) + ggtitle("a locus")
p.mta

library(cowplot)
p.mt <- plot_grid(p.mtb, p.mta)
p.mt

ggsave(p.mt, filename = "RecombinationLandscapeMatingTypeLocal.pdf", width = 8, height = 4)


#### Export map data for simulations ####

# Export the map of pop1 in HapMap format for use for simulations.
# All chromosomes are concatenated.
# 10kb windows are used.
# For simulations, we do not filter based on missing data, and chromosomes all start at 1.
# We therefore create a fake index:
nullindex <- data.frame(Chr = paste("chr", formatC(1:23, width = 2, flag = "0"), sep = ""), Start = 1)
map1.w10k <- read.mapper("ResultsDecoding/ABCPQRSTUV/Gamma/TwoKnots/Umaydis_ABCPQRSTUV", "10kb", 1.0, "ABCPQRSTUV", index = nullindex)

hapmap1 <- map1.w10k[,c("chrom", "chromStart", "sample_mean", "chromEnd", "ChrCode", "MeanPropMissing")]
hapmap1$WindowSize <- with(hapmap1, chromEnd - chromStart)

# Then we need to unscale rho to get cM/Mb:
hapmap1$Rec <- hapmap1$sample_mean * 1e6 * 100 * 5.23e-9 / 9.3e-4
hist(hapmap1$Rec)
summary(hapmap1$Rec) # Average of 17cM/Mb
hapmap1 <- hapmap1[, c("chrom", "Rec", "WindowSize", "chromStart", "chromEnd", "ChrCode", "MeanPropMissing")]

write.table(hapmap1, file = "ABCPQRSTUV.recmap.txt", row.names = FALSE, sep = "\t")


# Deprecated: smooth the map to make simulations (eventually) faster.
# If two consecutive windows differ by less than x%, we merge them and assign the average (weighted by their respective sizes).

reduceWindows <- function(map, relThreshold) {
  n <- nrow(map)
  # First compute all differences:
  d <- abs(map$Rec[-n] - map$Rec[-1]) / (map$Rec[-n] + map$Rec[-1])
  f <- (d < relThreshold)
  label <- 0
  map[1, "Group"] <- label
  for (i in 1:(n-1)) {
    if (!f[i]) {
      label <- label + 1
    } 
    map[1 + i, "Group"] <- label
  }
  map2 <- ddply(map, .variables = "Group", plyr::summarize,
                Chr = unique(chrom),
                Start = min(cum_start),
                Rec = (weighted.mean(Rec, WindowSize)),
                MeanPropMissing = weighted.mean(MeanPropMissing, WindowSize),
                ChromStart = min(chromStart),
                Size = sum(WindowSize))
  return(map2)
}

hapmap1s <- ddply(hapmap1, .variables = "chrom", reduceWindows, relThreshold = 0.5)
hapmap1s <- hapmap1s[, -c(1,2)]
# Reorder:
hapmap1s <- hapmap1s[order(hapmap1s$Start),]
weighted.mean(hapmap1s$Rec, hapmap1s$Size)

# Export as hapmap file (for msprime):
# We need to add a last entry with rec rate 0
hapmap1s[nrow(hapmap1s) + 1, "Start"] <- hapmap1s[nrow(hapmap1s), "Start"] + (map1.w10k[nrow(map1.w10k), "chromEnd"] - map1.w10k[nrow(map1.w10k), "chromStart"])
hapmap1s[nrow(hapmap1s), "Rec"] <- 0

write.table(hapmap1s, file = "ABCPQRSTUV.hapmap.txt", row.names = FALSE, sep = "\t")



#### Can variation of theta explain the results? ####

thetamap1.w10k <- read.table("Umaydis.iSMCstats10kb_nojust_ABCPQRSTUV.txt", header = TRUE, stringsAsFactors = FALSE)
thetamap1.w10k <- within(thetamap1.w10k, PropMissing <- (Counts.Unresolved + Counts.Gap) / (10*BlockLength))
thetamap1.w10k$ChrCode <- as.numeric(substring(thetamap1.w10k$Chr, 4))
thetamap1.w10k <- subset(thetamap1.w10k, !is.na(thetamap1.w10k$ChrCode))

thetamap2.w10k <- read.table("Umaydis.iSMCstats10kb_nojust_DEFGIKNO.txt", header = TRUE, stringsAsFactors = FALSE)
thetamap2.w10k <- within(thetamap2.w10k, PropMissing <- (Counts.Unresolved + Counts.Gap) / (10*BlockLength))
thetamap2.w10k$ChrCode <- as.numeric(substring(thetamap2.w10k$Chr, 4))
thetamap2.w10k <- subset(thetamap2.w10k, !is.na(thetamap2.w10k$ChrCode))

thetamap.w10k <- merge(thetamap1.w10k, thetamap2.w10k, by = c("Chr", "ChrCode", "Start", "Stop", "BlockLength", "BlockSize"))
plot(SequenceDiversityStatistics.TajimaPi.y~SequenceDiversityStatistics.TajimaPi.x, thetamap.w10k, subset = PropMissing.x < 0.5 & PropMissing.y < 0.5, log = "xy")
cor.test(~SequenceDiversityStatistics.TajimaPi.y+SequenceDiversityStatistics.TajimaPi.x, thetamap.w10k, subset = PropMissing.x < 0.5 & PropMissing.y < 0.5, method = "kendall")
# ***

thetamap1.w10k <- merge(thetamap1.w10k, map1.w10k, by.x = c("ChrCode", "Start", "Stop"), by.y = c("ChrCode", "chromStart", "chromEnd"))
thetamap2.w10k <- merge(thetamap2.w10k, map2.w10k, by.x = c("ChrCode", "Start", "Stop"), by.y = c("ChrCode", "chromStart", "chromEnd"))

plot(sample_mean~SequenceDiversityStatistics.TajimaPi, thetamap1.w10k, subset = PropMissing < 0.5, log="xy")
plot(sample_mean~SequenceDiversityStatistics.TajimaPi, thetamap2.w10k, subset = PropMissing < 0.5, log="xy")

# :( indeed