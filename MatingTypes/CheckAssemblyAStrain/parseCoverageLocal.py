#! /usr/bin/python

import gzip
import sys
import re
from collections import Counter

# This provides a detailed, per bp info:
print("Chr\tPos\tCoverage\tnbA\tnbC\tnbG\tnbT")
with gzip.open(sys.argv[1], "rt") as input_handle:
    for line in input_handle:
        values = line.split("\t")
        chr = values[0]
        pos = values[1]
        ref = values[2]
        seq = values[4]
        if chr == sys.argv[2]:
            seq2 = re.sub(r'[^ACGTacgt\\,\\.]', '', seq)
            seq3 = re.sub(r'[\\,\\.]', ref, seq2)
            coverage = len(seq3)
            counts = Counter(seq3)
            print("%s\t%s\t%s\t%s\t%s\t%s\t%s" % (chr, pos, coverage, counts['A'], counts['C'], counts['G'], counts['T']))


